<%-- 
    Document   : members
    Created on : 12-Dec-2014, 10:09:14
    Author     : Stephen
--%>

<%@page import="com.basic.Login"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<String> email = new ArrayList<String>();
    ArrayList<String> phone = new ArrayList<String>();
    ArrayList<String> address = new ArrayList<String>();
    ArrayList<Integer> permissions = new ArrayList<Integer>();
    permissions = (ArrayList<Integer>) session.getAttribute("permissions");
    String id = "" + session.getAttribute("currentSessionId");
    Login li = new Login();
    list = li.getMembers();
    email = li.getMembersEmail();
    phone = li.getMembersPhone();
    address = li.getMembersAddress();
    String mem = "";
    String ema = "";
    String phon = "";
    String addr = "";

    for (int o = 0; o < list.size(); o++) {
        if (o == list.size() - 1) {
            mem += list.get(o);
            ema += email.get(o);
            phon += phone.get(o);
            addr += address.get(o);
        } else {
            mem += list.get(o) + ",";
            ema += email.get(o) + ",";
            phon += phone.get(o) + "~";
            addr += address.get(o) + "~";
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.min.js" type="text/javascript"></script>
        <script src="scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic-Transact</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="WEB-INF/static_pages/adminHeader.jsp"></jsp:include>
                <div id="content">
                    <div id="profile">Members
                        <span class="links">
                            <span class="link2">Profile</span>
                            <span class="link2" id='adn'>Add new</span>
                            <span class="link2" id="mem">All Members</span>
                        </span>
                    </div>
                    <div id="Allmembers" class="contain">
                        <div id="search">
                            <div class="parts">
                                <div class="desc">Member Name</div>
                                <input type="text" name="ben" value="" class="txt2"/>
                            </div>
                            <div class="parts">
                                <div class="desc">E-mail address</div>
                                <input type="text" name="ben" value="" class="txt2"/>
                            </div>
                            <div class="parts">
                                <div class="desc">.</div>
                                <input type="submit" value="Search Member" name="btnTrn" class="btn"/>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div id="all">
                            <div class="titles">Members</div>
                            <div id="cov">
                                <div id="list">
                                    <ul>
                                    <% for (int i = 0; i < list.size(); i++) {%>
                                    <li class="list"><%= list.get(i)%></li><%}%>
                                </ul>
                            </div>
                        </div>
                        <div id="pers">
                            <div class="titles">Personal Details<span class="link2" style="margin-top: -7px" id="edlist">Edit</span></div>
                            <div id="pic" class="center"></div>
                            <div id="pros">
                                <div class="row"><span class="init">Name:</span><span class="res" id="nam">N/A</span></div>
                                <div class="row"><span class="init">E-mail Address:</span> <span class="res" id="ema">N/A</span></div>
                                <div class="row"><span class="init">Age:</span><span class="res">24</span></div>
                                <div class="row"><span class="init">Phone Number:</span><span class="res" id="ph">N/A</span></div>
                                <div class="row"><span class="init">Home Address:</span><span class="res" id="ad">N/A</span></div>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <div style="clear: both"></div>
                        <input type="hidden" name="members" value="<%= mem%>" id="members"/>
                        <input type="hidden" name="email" value="<%= ema%>" id="email"/>
                        <input type="hidden" name="ph" value="<%= phon%>" id="phn"/>
                        <input type="hidden" name="adres" value="<%= addr%>" id="adr"/>
                        <input type="hidden" name="agent" value="<%= id%>" id="fragent2"/>
                    </div>
                </div>
                <div id='adnew'>
                    <div id="profile" style="font-size: 15px">Create new User account</div>
                    <jsp:include page="WEB-INF/static_pages/createUser.jsp">
                        <jsp:param name="type" value="one" />
                    </jsp:include>
                </div>
                <div id="pro" class="contain">
                    <div class="titles">Account Settings</div>
                    <input type="hidden" name="inam" value="" id="acnam2"/>
                    <% if (permissions.contains(18)) {%><%@include file="WEB-INF/jspf/accuntInfo.jspf" %><%}%>
                    <% if (permissions.contains(22)) {%><%@include file="WEB-INF/jspf/setOverDraft.jspf"%><%}%>
                    <% if (permissions.contains(21)) {%><%@include file="WEB-INF/jspf/setDailyLimit.jspf" %><%}%>
                    <div class="titles">Login</div>
                    <% if (permissions.contains(8)) {%><%@include file="WEB-INF/jspf/manageLogPass.jspf" %><%}%>
                    <% if (permissions.contains(49)) {%><%@include file="WEB-INF/jspf/changePermGroup.jspf" %><%}%>
                    <div class="titles">Notifications</div>
                    <%@include file="WEB-INF/jspf/manageNotifications.jspf" %>
                    <%@include file="WEB-INF/jspf/sendMessage.jspf" %>
                    <%@include file="WEB-INF/jspf/sendEmail.jspf" %>
                    <div class="titles">Invoices</div>
                </div>
            </div>
            <jsp:include page="WEB-INF/static_pages/footer.jsp"></jsp:include>
        </div>
    </body>
</html>