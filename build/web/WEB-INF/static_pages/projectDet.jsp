<%-- 
    Document   : projectDet
    Created on : 21-Jan-2015, 10:04:46
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="content">
    <div id="profile">Projects
        <span class="links">
            <span class="link2b">Details</span>
            <a href="${pageContext.request.contextPath}/ProjectServlet"><span class="link2" id="mem">Projects</span></a>
        </span>
    </div>
        <%@include file="../jspf/editproject.jspf"%>
        <%@include file="../jspf/editbudget.jspf"%>
        <%@include file="../jspf/subprojects.jspf"%>
</div>