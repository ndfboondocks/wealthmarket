<%-- 
    Document   : agentWelcome
    Created on : 29-Dec-2014, 09:38:55
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String as = ""+session.getAttribute("currentSessionUser");
%>
<div id="content">
    <div id="profile">Hi <%= as%></div>
    <div class="content">Congratulations! you have been successfully licensed to be a Wealth Market Agent. You can now register your staff who would 
    take responsibility for performing all dealings with your clients. This account however is not allowed to undertake such duties but will rather 
    serve as your company's administrative account. Enjoy!</div>
    <div class="titles2">Get Started</div>
    <div class='content'>
        You can perform the following tasks:<br />
        <ul>
            <li class="blue2">Add Staff</li>
            <li class="blue2">Manage Staff</li>
            <li class="blue2">Manage Permissions</li>
            <li class="blue2">Set Permission Groups</li>
        </ul>
    </div>
</div>
