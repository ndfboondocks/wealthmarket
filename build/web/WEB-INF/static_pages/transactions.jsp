<%-- 
    Document   : transactions
    Created on : 09-Jan-2015, 18:02:58
    Author     : Stephen
--%>

<%@page import="com.basic.Login"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<Integer> amnt = new ArrayList<Integer>();
    Login li = new Login();
    list = li.getCashTransactions();
    amnt = li.getCTAmount();
%>
<div id="content">
    <div id="profile">Transactions
        <span class="links">
            <span class="link2" id='adn'>Warrants</span>
            <span class="link2">Services</span>
            <span class="link2">Goods</span>
            <span class="link2" id="mem">Cash</span>
        </span>
    </div>
    <div id="Allmembers" class="contain">
        <div class="titles">Sell Warrants for Cash</div>
        <div id="search">
            <form name="post" action="${pageContext.request.contextPath}/LedgerPost" method="POST">
                <div class="parts">
                    <div class="desc">Profile</div>
                    <select name="profile" class="drop">
                        <option>200 for 67</option>
                    </select>
                </div>
                <div class="parts">
                    <div class="desc">Amounts (cash)</div>
                    <input type="text" name="am" value="" class="txt2"/>
                </div>
                <div class="parts">
                    <div class="desc">Subscriber Account Number</div>
                    <input type="text" name="memid" value="" class="txt2"/>
                    <input type="submit" value="Transact" name="btnTrn" class="btn"/>
                </div>
            </form>
            <div style="clear: both"></div>
        </div>
        <div id="all">
            <div class="titles">Recent Transactions</div>
            <div id="list">
                <form name="getdetails" action="${pageContext.request.contextPath}/TransactionsServlet" method="POST">
                    <div>
                        <%
                        int g = list.size();
                        if (g < 1) {%>
                        <div class="list0">No Project added yet</div>
                            <%} else {
                        for (int i = 0; i < list.size(); i++) {%>
                            <input type="submit" value="<%= list.get(i)%>" name="btnget" class="listbtn"/>
                            <%}
                            }%>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id='adnew'>
    </div>
</div>
