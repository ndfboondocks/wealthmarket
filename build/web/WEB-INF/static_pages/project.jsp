<%-- 
    Document   : project
    Created on : 19-Jan-2015, 18:12:07
    Author     : Stephen
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> list = new ArrayList<String>();
    list = (ArrayList<String>) session.getAttribute("names");
    ArrayList<Integer> permissions = new ArrayList<Integer>();
    permissions = (ArrayList<Integer>) session.getAttribute("permissions");
%>
<div id="content">
    <div id="profile">Projects
        <span class="links">
            <span class="link2">Details</span>
            <span class="link2" id="adn">Add new</span>
            <span class="link2" id="mem">Projects</span>
        </span>
    </div>
    <div id="Allmembers" class="contain">
        <div id="search">
            <div class="parts">
                <div class="desc">Project Name</div>
                <input type="text" name="ben" value="" class="txtproj"/>
            </div>
            <div class="parts">
                <div class="desc">.</div>
                <input type="submit" value="Search Project" name="btnTrn" class="btn"/>
            </div>
            <div style="clear: both"></div>
        </div>
        <div id="all">
            <div class="titles">Projects</div>
            <div id="list">
                <form name="getdetails" action="${pageContext.request.contextPath}/ProjectServlet" method="POST">
                    <div>
                        <%
                        int g = list.size();
                        if (g < 1) {%>
                        <div class="list0">No Project added yet</div>
                            <%} else {
                        for (int i = 0; i < list.size(); i++) {%>
                            <input type="submit" value="<%= list.get(i)%>" name="btnget" class="listbtn"/>
                            <%}
                            }%>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="adnew">
        <% if(permissions.contains(63)){%><%@include file="../jspf/addproject.jspf"%>
        <%} else {%><div class="content"><div class="list0">Sorry You are not authorised to view this page</div></div><%}%>
    </div>
</div>