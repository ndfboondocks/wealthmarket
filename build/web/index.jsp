<%-- 
    Document   : index
    Created on : 25-Nov-2014, 11:02:33
    Author     : Stephen
    Note       : This is the First Page in the application which contains the log in form as well as other useful information on the Website
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="scripts/Main.css" rel="stylesheet" type="text/css"/>
        <link href="scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic-Transact</title>
    </head>
    <body>
        <div id="wrap">
            <div id="logPan">
<!--This is the LogIn container-->
                <form name="log" action="DataServe" method="POST">
                    <!--Post the Log in details entered into the "Dataserve" servlet-->
                    <div class="line">Basic-Transact</div>
                    <div class="line" style="margin-top: 1px">
                        <div class="desc" style="margin-top: 10px">Email</div>
                        <input type="text" name="pass" value="" class="txt" id="emailad"/>
                    </div>
                    <div class="line" style="margin-top: 1px">
                        <div class="desc" style="margin-top: 10px">Password</div>
                        <input type="password" name="user" value="" class="txt"/>
                    </div>
                    <div class="line"><input type="submit" value="Sign In" name="btnSub" class="btn" id="but"/></div>
                </form>
                <div class="logtitle">don't have an account?</div>
                <form name="create" action="createUser.jsp" method="POST">
                    <div class="line" style="margin-top: 10px"><input type="submit" value="Become a Member" name="btnCreate" class="btn"/></div>
                </form>
            </div>
        </div>
    </body>
</html>
