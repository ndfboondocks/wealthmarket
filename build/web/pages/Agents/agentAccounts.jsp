<%-- 
    Document   : agentAccounts
    Created on : 04-Feb-2015, 15:36:03
    Author     : Stephen
--%>
<%@page import="com.basic.ReadExcel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="../../scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="../../scripts/jquery-ui.js" type="text/javascript"></script>
        <script src="../../scripts/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic Transact</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../../WEB-INF/static_pages/adminHeader.jsp"></jsp:include>
            <jsp:include page="../../WEB-INF/static_pages/agentAccounts.jsp"></jsp:include>
            <jsp:include page="../../WEB-INF/static_pages/footer.jsp"></jsp:include>
        </div>
    </body>
</html>