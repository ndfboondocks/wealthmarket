<%-- 
    Document   : agentProject
    Created on : 19-Jan-2015, 18:09:40
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String type = "" + session.getAttribute("type");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="../../scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="../../scripts/jquery-ui.js" type="text/javascript"></script>
        <script src="../../scripts/jquery-ui.min.js" type="text/javascript"></script>
        <script src="../../scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic Transact</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../../WEB-INF/static_pages/adminHeader.jsp"></jsp:include>
            <%
                if (type.equals("one")) {
            %>
            <jsp:include page="../../WEB-INF/static_pages/project.jsp"></jsp:include>
            <%} else if (type.equals("two")) {%>
            <jsp:include page="../../WEB-INF/static_pages/projectDet.jsp"></jsp:include>
            <%}%>
            <jsp:include page="../../WEB-INF/static_pages/footer.jsp"></jsp:include>
        </div>
    </body>
</html>
