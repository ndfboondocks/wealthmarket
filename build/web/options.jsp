<%-- 
    Document   : options
    Created on : 17-Feb-2015, 15:19:53
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.min.js" type="text/javascript"></script>
        <script src="scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic-Transact</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="WEB-INF/static_pages/banner.jsp"></jsp:include>
                <div id="content">
                    <div id="profile">
                        Hi <%= "" + session.getAttribute("currentSessionUser")%>
                </div>
                <div class="content">Welcome to the Wealth Market. Enjoy!</div>
                <div class="titles2">Get Started</div>
                <div class='content'>
                    Please Select one account to view. <br />
                    <ul>
                        <li class="blue2">"User account" links to your personal wealth market accounts and other personal information and settings </li>
                        <li class="blue2">"Admin Account" links to an interface that allows you to execute your rights on behalf of your agency!</li>
                    </ul>
                </div>
                <div class="content">
                    <a href="pages/Members/home.jsp"><div class="usicon"></div></a>
                        <% String type = "" + session.getAttribute("typo");
                        if (type.equals("agent")) {%>
                    <a href="members.jsp"><div class="adicon"></div></a>
                        <% } else {%>
                    <a href="pages/AgentAdmin/agentAdminHome.jsp"><div class="adicon"></div></a>
                        <% }%>
                    <div style="clear: both"></div>
                </div>
                <div class="titles2">User Account</div>
                <div class='content'>
                    <div class="blue2">Your "user account" consists of all Warrant accounts held in the Wealth Market plc. along with your personal Notifications,
                    Messages and settings. <br /> You are able to perform transactions, buy and sell goods and services and also conduct other
                    financial or developmental activities, such as are allowed under the Wealth Market infrastructure.</div>
                </div>
                <div class="titles2">Agent Account</div>
                <div class='content'>
                    <div class="blue2">Your "Agent account" consists of all settings, permissions, and management tools that you have been 
                    assigned either as an agent of the Wealth Market plc, or as a "Staff" of an already existing agent of the Wealth Market plc.</div>
                </div>
            </div>
            <jsp:include page="WEB-INF/static_pages/footer.jsp"></jsp:include>
        </div>
    </body>
</html>
