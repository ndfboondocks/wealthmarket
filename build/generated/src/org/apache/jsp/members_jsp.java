package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.basic.Login;
import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;

public final class members_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(8);
    _jspx_dependants.add("/WEB-INF/jspf/accuntInfo.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/setOverDraft.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/setDailyLimit.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/manageLogPass.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/changePermGroup.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/manageNotifications.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/sendMessage.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/sendEmail.jspf");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    ArrayList<String> list = new ArrayList<String>();
    ArrayList<String> email = new ArrayList<String>();
    ArrayList<String> phone = new ArrayList<String>();
    ArrayList<String> address = new ArrayList<String>();
    ArrayList<Integer> permissions = new ArrayList<Integer>();
    permissions = (ArrayList<Integer>) session.getAttribute("permissions");
    String id = "" + session.getAttribute("currentSessionId");
    Login li = new Login();
    list = li.getMembers();
    email = li.getMembersEmail();
    phone = li.getMembersPhone();
    address = li.getMembersAddress();
    String mem = "";
    String ema = "";
    String phon = "";
    String addr = "";

    for (int o = 0; o < list.size(); o++) {
        if (o == list.size() - 1) {
            mem += list.get(o);
            ema += email.get(o);
            phon += phone.get(o);
            addr += address.get(o);
        } else {
            mem += list.get(o) + ",";
            ema += email.get(o) + ",";
            phon += phone.get(o) + "~";
            addr += address.get(o) + "~";
        }
    }

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link href=\"scripts/Main.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <script src=\"scripts/jquery-1.6.1.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"scripts/jquery-ui.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"scripts/jquery-ui.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"scripts/mainscript.js\" type=\"text/javascript\"></script>\n");
      out.write("        <title>Basic-Transact</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("            ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "../../WEB-INF/static_pages/adminHeader.jsp", out, false);
      out.write("\n");
      out.write("                <div id=\"content\">\n");
      out.write("                    <div id=\"profile\">Members\n");
      out.write("                        <span class=\"links\">\n");
      out.write("                            <span class=\"link2\">Profile</span>\n");
      out.write("                            <span class=\"link2\" id='adn'>Add new</span>\n");
      out.write("                            <span class=\"link2\" id=\"mem\">All Members</span>\n");
      out.write("                        </span>\n");
      out.write("                    </div>\n");
      out.write("                    <div id=\"Allmembers\" class=\"contain\">\n");
      out.write("                        <div id=\"search\">\n");
      out.write("                            <div class=\"parts\">\n");
      out.write("                                <div class=\"desc\">Member Name</div>\n");
      out.write("                                <input type=\"text\" name=\"ben\" value=\"\" class=\"txt2\"/>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"parts\">\n");
      out.write("                                <div class=\"desc\">E-mail address</div>\n");
      out.write("                                <input type=\"text\" name=\"ben\" value=\"\" class=\"txt2\"/>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"parts\">\n");
      out.write("                                <div class=\"desc\">.</div>\n");
      out.write("                                <input type=\"submit\" value=\"Search Member\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("                            </div>\n");
      out.write("                            <div style=\"clear: both\"></div>\n");
      out.write("                        </div>\n");
      out.write("                        <div id=\"all\">\n");
      out.write("                            <div class=\"titles\">Members</div>\n");
      out.write("                            <div id=\"cov\">\n");
      out.write("                                <div id=\"list\">\n");
      out.write("                                    <ul>\n");
      out.write("                                    ");
 for (int i = 0; i < list.size(); i++) {
      out.write("\n");
      out.write("                                    <li class=\"list\">");
      out.print( list.get(i));
      out.write("</li>");
}
      out.write("\n");
      out.write("                                </ul>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div id=\"pers\">\n");
      out.write("                            <div class=\"titles\">Personal Details<span class=\"link2\" style=\"margin-top: -7px\" id=\"edlist\">Edit</span></div>\n");
      out.write("                            <div id=\"pic\" class=\"center\"></div>\n");
      out.write("                            <div id=\"pros\">\n");
      out.write("                                <div class=\"row\"><span class=\"init\">Name:</span><span class=\"res\" id=\"nam\">N/A</span></div>\n");
      out.write("                                <div class=\"row\"><span class=\"init\">E-mail Address:</span> <span class=\"res\" id=\"ema\">N/A</span></div>\n");
      out.write("                                <div class=\"row\"><span class=\"init\">Age:</span><span class=\"res\">24</span></div>\n");
      out.write("                                <div class=\"row\"><span class=\"init\">Phone Number:</span><span class=\"res\" id=\"ph\">N/A</span></div>\n");
      out.write("                                <div class=\"row\"><span class=\"init\">Home Address:</span><span class=\"res\" id=\"ad\">N/A</span></div>\n");
      out.write("                            </div>\n");
      out.write("                            <div style=\"clear: both\"></div>\n");
      out.write("                        </div>\n");
      out.write("                        <div style=\"clear: both\"></div>\n");
      out.write("                        <input type=\"hidden\" name=\"members\" value=\"");
      out.print( mem);
      out.write("\" id=\"members\"/>\n");
      out.write("                        <input type=\"hidden\" name=\"email\" value=\"");
      out.print( ema);
      out.write("\" id=\"email\"/>\n");
      out.write("                        <input type=\"hidden\" name=\"ph\" value=\"");
      out.print( phon);
      out.write("\" id=\"phn\"/>\n");
      out.write("                        <input type=\"hidden\" name=\"adres\" value=\"");
      out.print( addr);
      out.write("\" id=\"adr\"/>\n");
      out.write("                        <input type=\"hidden\" name=\"agent\" value=\"");
      out.print( id);
      out.write("\" id=\"fragent2\"/>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div id='adnew'>\n");
      out.write("                    <div id=\"profile\" style=\"font-size: 15px\">Create new User account</div>\n");
      out.write("                    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "WEB-INF/static_pages/createUser.jsp" + "?" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("type", request.getCharacterEncoding())+ "=" + org.apache.jasper.runtime.JspRuntimeLibrary.URLEncode("one", request.getCharacterEncoding()), out, false);
      out.write("\n");
      out.write("                </div>\n");
      out.write("                <div id=\"pro\" class=\"contain\">\n");
      out.write("                    <div class=\"titles\">Account Settings</div>\n");
      out.write("                    <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam2\"/>\n");
      out.write("                    ");
 if (permissions.contains(18)) {
      out.write("\n");
      out.write("<!--This is the section provides the fields for setting limits on accounts-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\">\n");
      out.write("        <div class=\"subHead\">Account Information</div>\n");
      out.write("        <div class=\"subCont1\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Account Name</div>\n");
      out.write("                <div class=\"dis\" id=\"name\"></div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Status</div>\n");
      out.write("                <div class=\"dis\">Active</div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Available Balance</div>\n");
      out.write("                <div class=\"dis\">20,000</div>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
}
      out.write("\n");
      out.write("                    ");
 if (permissions.contains(22)) {
      out.write("\n");
      out.write("<!--This is the section provides the fields for setting Overdraft limits on an account-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Set Overdraft</div>\n");
      out.write("        <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("        <input type=\"hidden\" name=\"agid\" value=\"\" id=\"agentid2\"/>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Overdraft Amount</div>\n");
      out.write("                <input type=\"text\" name=\"od\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Expiry date</div>\n");
      out.write("                <input type=\"text\" name=\"dat\" value=\"\" class=\"txt2\" id=\"startDatePicker\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Description</div>\n");
      out.write("                <input type=\"text\" name=\"desc\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">.</div>\n");
      out.write("                <input type=\"submit\" value=\"Set Overdraft\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
}
      out.write("\n");
      out.write("                    ");
 if (permissions.contains(21)) {
      out.write("\n");
      out.write("<!--This is the section provides the fields for setting daily limits on an account-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Set Daily Limit</div>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Amount</div>\n");
      out.write("                <input type=\"text\" name=\"od\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Description</div>\n");
      out.write("                <input type=\"text\" name=\"desc\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">.</div>\n");
      out.write("                <input type=\"submit\" value=\"Set Limit\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
}
      out.write("\n");
      out.write("                    <div class=\"titles\">Login</div>\n");
      out.write("                    ");
 if (permissions.contains(8)) {
      out.write("\n");
      out.write("<!--This is the section provides the fields for changing the password of a user account-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Manage Login Password</div>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"ac\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">New Password</div>\n");
      out.write("                <input type=\"password\" name=\"pass\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Confirm New Password</div>\n");
      out.write("                <input type=\"password\" name=\"cpass\" value=\"\" class=\"txt2\" id=\"startDatePicker\"/>\n");
      out.write("                <input type=\"submit\" value=\"Change Password\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
}
      out.write("\n");
      out.write("                    ");
 if (permissions.contains(49)) {
      out.write("\n");
      out.write("<!--This is the section provides the fields for changing the permission group of a member or staff-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Change Permission Group</div>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Current Group</div>\n");
      out.write("                <input type=\"text\" name=\"od\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">New Group</div>\n");
      out.write("                <input type=\"text\" name=\"dat\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">Comment</div>\n");
      out.write("                <input type=\"text\" name=\"dat\" value=\"\" class=\"txt2\"/>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <div class=\"desc\">.</div>\n");
      out.write("                <input type=\"submit\" value=\"Change Permission Group\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
}
      out.write("\n");
      out.write("                    <div class=\"titles\">Notifications</div>\n");
      out.write("                    ");
      out.write("\n");
      out.write("<!--This is the section provides the fields for managing the notifications on a user account-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Manage Notifications</div>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <input type=\"submit\" value=\"Save Notifications\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
      out.write("\n");
      out.write("                    ");
      out.write("\n");
      out.write("<!--This is the section provides the fields for sending an internal mail to a user-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Send Message</div>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <input type=\"submit\" value=\"Send Message\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
      out.write("\n");
      out.write("                    ");
      out.write("\n");
      out.write("<!--This is the section provides the fields for sending an email to a user-->\n");
      out.write("\n");
      out.write("<form name=\"setod\" action=\"SetOD\" method=\"POST\">\n");
      out.write("    <div id=\"setOverdraft\" class=\"subSect\">\n");
      out.write("        <div class=\"subHead\">Send Email</div>\n");
      out.write("        <div class=\"subCont\">\n");
      out.write("            <input type=\"hidden\" name=\"inam\" value=\"\" id=\"acnam\"/>\n");
      out.write("            <div class=\"parts\">\n");
      out.write("                <input type=\"submit\" value=\"Send Email\" name=\"btnTrn\" class=\"btn\"/>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"clear: both\"></div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</form>\n");
      out.write("\n");
      out.write("                    <div class=\"titles\">Invoices</div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "../../WEB-INF/static_pages/footer.jsp", out, false);
      out.write("\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
