/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stephen
 */
public class DataServe extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        try (PrintWriter out = response.getWriter()) {
            String ans;
            HashMap<String, String> det = new HashMap<String, String>();
            String pass = request.getParameter("user");
            String ema = request.getParameter("pass");
            ans = Login.getUsers(pass, ema);

            if (ans.equals("member")) {
                int id = Login.getMemID(ema);
                det.clear();
                det = Login.getAccountDet(id);
                HttpSession session = request.getSession(true);

                session.setAttribute("sessionBal", det.get("balance"));
                session.setAttribute("sessionName", det.get("name"));
                session.setAttribute("sessionDate", det.get("created"));
                session.setAttribute("Id", det.get("id"));
                session.setAttribute("subclass", det.get("subclass"));
                session.setAttribute("blocked", det.get("blocked"));
                session.setAttribute("available", det.get("available"));
                session.setAttribute("sessId", det.get("account_id"));
                session.setAttribute("currentSessionUser", det.get("user"));
                if (null == session.getAttribute("sessionName")) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("Something went wrong!!!");
                    out.println("location='index.jsp';");
                    out.println("</script>");
                } else {
                    response.sendRedirect("pages/Members/home.jsp"); //logged-in page	                     
                }
            } else if (ans.equals("agent")) {
                int id = Login.getMemID(ema);
                det.clear();
                det = Login.getAccountDet(id);
                HttpSession session = request.getSession(true);

                session.setAttribute("sessionBal", det.get("balance"));
                session.setAttribute("sessionName", det.get("name"));
                session.setAttribute("sessionDate", det.get("created"));
                session.setAttribute("Id", det.get("id"));
                session.setAttribute("subclass", det.get("subclass"));
                session.setAttribute("blocked", det.get("blocked"));
                session.setAttribute("available", det.get("available"));
                session.setAttribute("sessId", det.get("account_id"));
                session.setAttribute("currentSessionUser", det.get("user"));
                det.clear();
                det = Login.getAgentDetails(id);
                session.setAttribute("currentSessionUser", det.get("name"));
                session.setAttribute("currentSessionId", det.get("id"));
                session.setAttribute("agentUserId", id);
                session.setAttribute("typo", "admin");
                //response.sendRedirect("pages/AgentAdmin/agentAdminHome.jsp");
                response.sendRedirect("options.jsp");
            } else if (ans.equals("staff")) {
                ArrayList<Integer> permlist = new ArrayList<Integer>();
                int id = Login.getMemID(ema);
                det.clear();
                det = Login.getAccountDet(id);
                HttpSession session = request.getSession(true);

                session.setAttribute("sessionBal", det.get("balance"));
                session.setAttribute("sessionName", det.get("name"));
                session.setAttribute("sessionDate", det.get("created"));
                session.setAttribute("Id", det.get("id"));
                session.setAttribute("subclass", det.get("subclass"));
                session.setAttribute("blocked", det.get("blocked"));
                session.setAttribute("available", det.get("available"));
                session.setAttribute("sessId", det.get("account_id"));
                session.setAttribute("currentSessionUser", det.get("user"));
                det.clear();
                det = Login.getStaffDetails(id);
                session.setAttribute("currentSessionUser", det.get("name"));
                session.setAttribute("currentSessionId", det.get("id"));
                session.setAttribute("typo", "agent");
                int agroup = Login.getStaffAgentGroupID(det.get("id"));
                int permid = Permissions.getStaffPermissionID(id);
                permlist = Permissions.getStaffPermissionIDList(permid, agroup);
                session.setAttribute("permissions", permlist);
                //response.sendRedirect("pages/Agents/agentHome.jsp");
                response.sendRedirect("options.jsp");
            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('User or password incorrect');");
                out.println("location='index.jsp';");
                out.println("</script>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataServe.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataServe.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataServe.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataServe.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
