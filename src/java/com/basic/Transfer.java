/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author Stephen
 */
public class Transfer {


    public Transfer(){
        
    }
    
    public static String Transfer(int curbal, String unam, int acno, int bal, int am, Date date, int mid, String Desc) throws ClassNotFoundException, SQLException{
        String result = "done";
        Connection con = null;
        PreparedStatement rstmt;
        PreparedStatement stmt;
        PreparedStatement istmt;
        ResultSet rs;
        String bdy = unam+" has just paid in =N"+bal+" to your account. "+unam+" said '"+Desc+"'";
        int baldiff = 0;
        if (curbal > 0) {
            baldiff = bal - curbal;
        }
        else{
            baldiff = bal;
        }
        /*create random values from the Random method*/
        int id = RandomNumber(9999, 1111);
        int id2 = RandomNumber(9999, 1111);
        /* Get the current date in a format that matches the sql syntax*/
        java.sql.Date dt = new java.sql.Date(date.getTime());

        try {
            /* create a connection to the database*/
            con = new JDBCConnector().getConnection();
            /*Query the database to get details of the user account*/
            rstmt = con.prepareStatement("Select * from accounts where id = ?");
            rstmt.setInt(1,mid);
            rs = rstmt.executeQuery();
            rs.first();
            int acno1 = rs.getInt("account_number");
            
            /* Query the database to find the beneficiary account*/
            rstmt = con.prepareStatement("Select * from accounts where account_number = ?");
            rstmt.setInt(1,acno);
            rs = rstmt.executeQuery();
            /*If the beneficiary account exists*/
            if (rs.first()) {
                int mid2 = rs.getInt("member_id");
                int idf = rs.getInt("id");
                String toac = rs.getString("owner_name");
                /* update account balances for both the user and beneficiary*/
                int bal2 = UpdateBalance(mid, am, 2);
                int bal1 = UpdateBalance(idf, bal, 1);
                /*send transfer message to beneficiary*/
                sendMemberMessage(mid2, bdy, "Payment Received");
                /* update overdraft balances for both the user and beneficiary*/
                UpdateOverdraft(mid, bal, 1);
                UpdateOverdraft(idf, bal, 2);
                
                /* record the transaction*/
                stmt = con.prepareStatement("INSERT INTO cyclos3.customtransfers (`type`, `Amount`, id, account_id, old_balance, new_balance, `time`, `Description`, `Date`, `OtherAccountNumber`, `OtherAccountName`) VALUES (?, ?, ?, ?, ?, ?, CURRENT_TIME, ?, CURRENT_DATE, ?, ?)");
                stmt.setString(1, "Debit");
                stmt.setInt(2, -bal);
                stmt.setInt(3, id);
                stmt.setInt(4, mid);
                stmt.setInt(5, bal2);
                stmt.setInt(6, bal2 - am);
                stmt.setString(7, Desc);
                stmt.setInt(8, acno);
                stmt.setString(9, toac);
                stmt.executeUpdate();   
                
                stmt.setString(1, "Credit");
                stmt.setInt(2, bal);
                stmt.setInt(3, id2);
                stmt.setInt(4, idf);    
                stmt.setInt(5, bal1);
                stmt.setInt(6, bal1 + bal);
                stmt.setString(7, Desc);
                stmt.setInt(8, acno1);
                stmt.setString(9, unam);
                stmt.executeUpdate();
            }
            else{
                result="invalid";
                return result;
            }
        } 
        catch (SQLException e) {
            result="failed";
        }
        finally{
            con.close();
        }
        return result;
    }
    
    public static String InsertTransfer(int curbal, String unam, int toac, int bal, Date date, int mid, String desc) throws ClassNotFoundException, SQLException{
        
        String res = "done";
        int am = 0;
        /* check if the user is trying to overdraw his/her account*/
        if (curbal < bal) {
            /* if account is being overdrawn, check if user possesses enough credit to cover the difference*/
            String net = checkIfValid(curbal, bal, mid);
            /*if the user possesses enough credit, make the transfer*/
            if (net.equals("accepted")) {
                res = Transfer(curbal, unam, toac, bal, bal, date, mid, desc);
            }
            /*If the user doesn't possess enough credit for the transaction*/
            else{
                res = "insufficient";
                return res;
            }
        }
        /*If the user's account contains more than the requested transfer amount*/
        else{
            /*perform the transfer*/
            res = Transfer(curbal, unam, toac, bal, bal, date, mid, desc);
        }
            return res;
    }
    
    public static String checkIfValid(int curbal, int bal, int mid) throws ClassNotFoundException, SQLException {
        String result = "denied";
        Connection con = null;
        PreparedStatement stmt;
        PreparedStatement istmt;
        ResultSet rs;
        int over, drawn, diff;
        /* Get the overdrawn amount*/
        if (curbal < 0) {
            curbal = 0;
        }
        int baldiff = bal - curbal;
        
        try {
            con = new JDBCConnector().getConnection();
            /* Query the database for the user's overdraft details*/
            stmt = con.prepareStatement("Select * from overdraft where account_id = ?");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            /* If the user has an Overdraft line*/
            if (rs.first()) {
                over = rs.getInt("Overdraft_line");
                drawn = rs.getInt("Overdrawn");
                /* Check how much Overdraft the user has left*/
                diff = over - drawn;
                /* If the user doesn't have enough to cover the transaction*/
                if (diff < baldiff) {
                    result = "denied";
                }
                /*If the user has enough to cover the transaction*/
                else{
                    result = "accepted";
                }
            }
            /* If the user doesn't have an Overdraft line*/
            else{
                result = "denied";
                return result;
            }
        }
        catch (SQLException e) {
            result = "denied";
        }
        finally{
            con.close();
        }
        return result;
    }
    
    public static String UpdateOverdraft(int mid, int amount, int fg) throws SQLException, ClassNotFoundException{
        Connection con = null;
        PreparedStatement istmt;
        PreparedStatement stmt;
        ResultSet rs;
        int over;
        int sum = 0;
        String result = "accepted";
        
        try {
            con = new JDBCConnector().getConnection();
            /* Query the database for the overdraft details of the user*/
            stmt = con.prepareStatement("Select * from overdraft where account_id = ?");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            /* If the user has an overdraft balance*/
            if (rs.first()) {
                /* Get the overdrawn amount*/
                over = rs.getInt("Overdrawn");
                /* For the user*/
                if (fg == 1) {
                    sum = over + amount;
                }
                /* For the beneficiary*/
                else if (fg == 2) {
                    if (over > 0) {
                        sum = over - amount;
                    } else {
                        sum = 0;
                    }
                }

                /* Update the overdrawn amount in the user's overdraft account*/
                istmt = con.prepareStatement("UPDATE overdraft set Overdrawn = ? where account_id = ?");
                istmt.setInt(1, sum);
                istmt.setInt(2, mid);
                istmt.executeUpdate();
            }
            else{
                result = "denied";
                return result;
            }
        }
        catch (SQLException e) {
            result = "denied";
        }
        finally{
            con.close();
        }
        return result;
    }
    
    public static int UpdateBalance(int acid, int up, int fm) throws ClassNotFoundException{
        Connection con = null;
        PreparedStatement stmt;
        PreparedStatement istmt;
        ResultSet rs;
        int blocked = 0;
        String result = "accepted";
        int bal = 0;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from account_fee_amounts where account_id = ?");
            stmt.setInt(1, acid);
            rs = stmt.executeQuery();
            rs.first();
            bal = rs.getInt("available_balance");
            int curblock = rs.getInt("blocked");
            int newbal = 0;
            if (fm == 1) {
                blocked = AmountToBeBlocked(acid, curblock, up);
                newbal = bal+up;
            }
            if (fm == 2) {
                newbal = bal-up;
            }
            istmt = con.prepareStatement("UPDATE account_fee_amounts set available_balance = ?, blocked = ? where account_id = ?");
            istmt.setInt(1, newbal);
            istmt.setInt(2, blocked);
            istmt.setInt(3, acid);
            istmt.executeUpdate();
        }
        catch (SQLException e) {
            result = "denied";
        }
        return bal;
    }
    
    private static int AmountToBeBlocked(int acid, int curblock, int up) throws ClassNotFoundException, SQLException {
        int amount = 0;
        Connection con = null;
        PreparedStatement stmt;
        PreparedStatement istmt;
        ResultSet rs;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from overdraft where account_id = ?");
            stmt.setInt(1, acid);
            rs = stmt.executeQuery();
            rs.first();
            int over = rs.getInt("Overdrawn");
            if (up > over) {
                amount = over + curblock;
            }
            else{
                amount = up + curblock;
            }
        }
        catch (SQLException e) {
            
        }
        finally{
            con.close();
        }
        return amount;
    }
    
    public static void sendMemberMessage(int id, String bdy, String sub) throws ClassNotFoundException, SQLException{
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        Date date = new Date();
        java.sql.Date dat = new java.sql.Date(date.getTime());
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.messages (`date`, subject, `type`, direction, is_read, is_replied, is_html, removed_at, from_member_id, to_member_id, category_id, body, email_sent) VALUES (CURRENT_TIMESTAMP, ?, 'msg', 'I', true, false, false, CURRENT_TIMESTAMP, NULL, ?, NULL, ?, true)");
            stmt.setString(1, sub);
            stmt.setInt(2, id);
            stmt.setString(3, bdy);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
        }
        finally{
            con.close();
        }
    }
    
    public static String TransferWarrants(int amount, int mid){
        String result = "";
        return result;
    }
    
    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
}


