/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stephen
 */
public class PrefServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String type = request.getParameter("type");
            if (type.equals("agent")) {
                String member = request.getParameter("mem");
                String group = request.getParameter("groups");
                int id = Login.getMemberID(member);
                String res = Account_Settings.addAgent(id, group);
                if (res.equals("success")) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Successful');");
                    out.println("location='pages/Agents/agentAccounts.jsp';");
                    out.println("</script>");
                    response.sendRedirect("pages/Agents/agentAccounts.jsp");
                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Denied');");
                    out.println("location='pages/Agents/agentAccounts.jsp';");
                    out.println("</script>");
                    response.sendRedirect("pages/Agents/agentAccounts.jsp");
                }
            } else {
                int memid = Integer.parseInt(request.getParameter("memid"));
                String pass = request.getParameter("pass");
                String cpass = request.getParameter("cpass");
                String res = "";
                if (pass.equals(cpass)) {
                    res = Account_Settings.changePassword(memid, pass);
                    if (res.equals("success")) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Password change was Successful');");
                        out.println("location='pages/Members/preferences.jsp';");
                        out.println("</script>");
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('"+res+"');");
                        out.println("location='pages/Members/preferences.jsp';");
                        out.println("</script>");
                    }
                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Password and confirmation must match');");
                    out.println("location='pages/Members/preferences.jsp';");
                    out.println("</script>");
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PrefServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PrefServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PrefServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PrefServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
