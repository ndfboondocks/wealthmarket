/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import sun.management.resources.agent;

/**
 *
 * @author Stephen
 */
public class Login {

    static ArrayList<String> user = new ArrayList<String>();
    static ArrayList<String> us = new ArrayList<String>();
    static ArrayList<String> mem = new ArrayList<String>();
    static ArrayList<String> messub = new ArrayList<String>();
    static ArrayList<String> mesbody = new ArrayList<String>();
    static ArrayList<String> mesdate = new ArrayList<String>();
    static ArrayList<String> sub = new ArrayList<String>();
    static ArrayList<String> memnam = new ArrayList<String>();
    static ArrayList<String> emal = new ArrayList<String>();
    static ArrayList<String> phone = new ArrayList<String>();
    static ArrayList<String> addr = new ArrayList<String>();
    static ArrayList<String> agnam = new ArrayList<String>();
    static ArrayList<String> stafnam = new ArrayList<String>();
    static ArrayList<String> stafpass = new ArrayList<String>();
    static ArrayList<String> agId = new ArrayList<String>();
    static ArrayList<String> agpass = new ArrayList<String>();
    static ArrayList<String> email = new ArrayList<String>();
    static HashMap<String, String> Details = new HashMap<String, String>();
    static HashMap<String, String> agDetails = new HashMap<String, String>();
    static ArrayList<Integer> memberID = new ArrayList<Integer>();
    static ArrayList<Integer> ctamt = new ArrayList<Integer>();
    static int curr, rp1, rp;
    static String subclass;
    static String curruser;
    static int Meid;

    public Login() {

    }

    public static String getUsers(String pass, String ema) throws ClassNotFoundException, SQLException {
        String result = "invalid";
        emal.clear();
        mem.clear();
        us.clear();
        user.clear();
        sub.clear();
        email.clear();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members");
            rs = stmt.executeQuery();
            rs.first();
            email.add(rs.getString("email"));
            memberID.add(rs.getInt("id"));
            sub.add(rs.getString("subclass"));
            us.add(rs.getString("name"));
            while (rs.next()) {
                email.add(rs.getString("email"));
                sub.add(rs.getString("subclass"));
                memberID.add(rs.getInt("id"));
                us.add(rs.getString("name"));
            }

            stmt = con.prepareStatement("Select * from users");
            rs = stmt.executeQuery();
            rs.first();
            user.add(rs.getString("password"));
            while (rs.next()) {
                user.add(rs.getString("password"));
            }

            stmt = con.prepareStatement("Select * from members where subclass = ?");
            stmt.setString(1, "M");
            rs = stmt.executeQuery();
            rs.first();
            emal.add(rs.getString("email"));
            mem.add(rs.getString("name"));
            while (rs.next()) {
                emal.add(rs.getString("email"));
                mem.add(rs.getString("name"));
            }

            stmt = con.prepareStatement("Select * from users where subclass = ?");
            stmt.setString(1, "M");
            rs = stmt.executeQuery();
            rs.first();
            phone.add(rs.getString("phone_number"));
            while (rs.next()) {
                phone.add(rs.getString("phone_number"));
            }
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        for (int i = 0; i < email.size(); i++) {
            if (email.get(i).equals(ema) && user.get(i).equals(pass)) {
                result = "member";
                curr = memberID.get(i);
                curruser = us.get(i);
                result = checkAgent(memberID.get(i));
                return result;
            }
        }
        return result;
    }

    public static String checkAgent(int id) throws ClassNotFoundException, SQLException {
        String result = "member";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from agents where Id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            if (rs.first()) {
                result = "agent";
            } else{
                result = checkStaff(id);
            }
        } catch (SQLException e) {

        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }

    public static String checkStaff(int id) throws ClassNotFoundException, SQLException {
        String result = "member";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from staff where Id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            if (rs.first()) {
                result = "staff";
            }
        } catch (SQLException e) {

        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }

    public static ArrayList<String> getStaffList(String agent) throws ClassNotFoundException, SQLException {
        agnam.clear();
        String agid;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from agents where agent_name = ?");
            stmt.setString(1, agent);
            rs = stmt.executeQuery();
            rs.first();
            agid = rs.getString("agent_Id");

            stmt = con.prepareStatement("Select * from staff where agent_Id = ?");
            stmt.setString(1, agid);
            rs = stmt.executeQuery();
            rs.first();
            agnam.add(rs.getString("staff_name"));
            while (rs.next()) {
                agnam.add(rs.getString("staff_name"));
            }
        } catch (SQLException e) {

        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return agnam;
    }

    public static HashMap<String, String> getAgentDetails(int id) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        agDetails.clear();
        String det = "";
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from agents where Id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            agDetails.put("name", rs.getString("agent_name"));
            agDetails.put("id", rs.getString("agent_Id"));
        } catch (SQLException e) {
            det = e.getMessage();
        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return agDetails;
    }
    
    public static HashMap<String, String> getStaffDetails(int id) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        agDetails.clear();
        String det = "";
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from staff where Id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            agDetails.put("name", rs.getString("staff_name"));
            agDetails.put("id", rs.getString("agent_Id"));
        } catch (SQLException e) {
            det = e.getMessage();
        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return agDetails;
    }
    
    public static int getStaffAgentGroupID(String agid) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int grid = 0;
        String det = "";
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from agents where agent_Id = ?");
            stmt.setString(1, agid);
            rs = stmt.executeQuery();
            rs.first();
            int id = rs.getInt("Id");
            
            stmt = con.prepareStatement("Select * from users where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            grid = rs.getInt("group_id");
        } catch (SQLException e) {
            det = e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return grid;
    }

    public static ArrayList<String> getMembers() throws ClassNotFoundException, SQLException {
        mem.clear();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members where subclass = ?");
            stmt.setString(1, "M");
            rs = stmt.executeQuery();
            rs.first();
            mem.add(rs.getString("name"));
            while (rs.next()) {
                mem.add(rs.getString("name"));
            } 
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return mem;
    }
    
    public static int getMemberID(String name) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = 0;
        String res = "";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members where name = ?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            rs.first();
            id = rs.getInt("id");
        } catch (SQLException e) {
            res = e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        res += name; 
        return id;
    }

    public static ArrayList<String> getStaff() throws ClassNotFoundException {
        return mem;
    }

    public static ArrayList<String> getMembersEmail() throws ClassNotFoundException, SQLException {
        emal.clear();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members");
            rs = stmt.executeQuery();
            rs.first();
            emal.add(rs.getString("email"));
            while (rs.next()) {
                emal.add(rs.getString("email"));
            } 
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return emal;
    }

    public static ArrayList<String> getMembersAddress() throws ClassNotFoundException, SQLException {
        ArrayList<String> add = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from users");
            rs = stmt.executeQuery();
            rs.first();
            add.add(""+rs.getInt("street_id"));
            while (rs.next()) {
                add.add(""+rs.getInt("street_id"));
            }
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return add;
    }

    public static ArrayList<String> getMembersPhone() throws ClassNotFoundException, SQLException {
        phone.clear();
        addr.clear();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from users");
            rs = stmt.executeQuery();
            rs.first();
            phone.add(rs.getString("phone_number"));
            while (rs.next()) {
                phone.add(rs.getString("phone_number"));
            }
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return phone;
    }

    public static ArrayList<String> getMemberName() {
        return emal;
    }

    public static HashMap<String, String> getAccountDet(int memid) throws ClassNotFoundException, SQLException {
        Details.clear();
        memnam.clear();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        PreparedStatement istmt;
        ResultSet rst;
        int useID;
        int bal;
        int abal = 0;
        int odl, over;
        Date sdt = new Date();
        java.sql.Date dt = new java.sql.Date(sdt.getTime());
        String nm;
        int mid;
        int block = 0;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from accounts where member_id = ?");
            stmt.setInt(1, memid);
            rs = stmt.executeQuery();
            rs.first();
            useID = rs.getInt("id");
            nm = rs.getString("owner_name");
            memnam.add(rs.getString("owner_name"));
            mid = rs.getInt("member_id");
            subclass = rs.getString("subclass");

            istmt = con.prepareStatement("Select * from account_fee_amounts where account_id = ?");
            istmt.setInt(1, useID);
            rst = istmt.executeQuery();
            rst.first();
            bal = rst.getInt("available_balance");
            dt = rst.getDate("date");
            block = rst.getInt("blocked");
            abal = bal - block;

            istmt = con.prepareStatement("Select * from overdraft where account_id = ?");
            istmt.setInt(1, useID);
            rst = istmt.executeQuery();
            if (rst.first()) {
                odl = rst.getInt("Overdraft_line");
                over = rst.getInt("Overdrawn");
                if (abal < 0) {
                    int newb = odl - over;
                    abal = newb - block;
                } else {
                    abal += odl - over;
                }
            }

            Details.put("balance", "" + bal);
            Details.put("user", curruser);
            Details.put("name", nm);
            Details.put("created", "" + dt);
            Details.put("id", "" + mid);
            Details.put("account_id", "" + useID);
            Details.put("subclass", subclass);
            Details.put("blocked", "" + block);
            Details.put("available", "" + abal);
        } catch (SQLException e) {

        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return Details;
    }

    public static int getMemID(String email) throws ClassNotFoundException, SQLException{
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members where email = ?");
            stmt.setString(1, email);
            rs = stmt.executeQuery();
            rs.first();
            id = rs.getInt("id");
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return id;
    }
    
    public static ArrayList<String> getMessages(int meid) throws ClassNotFoundException, SQLException {
        messub.clear();
        mesbody.clear();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from messages where to_member_id = ?");
            stmt.setInt(1, meid);
            rs = stmt.executeQuery();
            rs.first();
            messub.add(rs.getString("subject"));
            mesbody.add(rs.getString("body"));
            while (rs.next()) {
                messub.add(rs.getString("subject"));
                mesbody.add(rs.getString("body"));
            }
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return messub;
    }

    public static ArrayList<String> getMessageBody() {
        return mesbody;
    }
    
    public static int getAccountId(int acno) throws ClassNotFoundException, SQLException{
        int acid = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from accounts where account_number = ?");
            stmt.setInt(1, acno);
            rs = stmt.executeQuery();
            rs.first();
            acid = rs.getInt("id");
        } catch (SQLException e) {
            return acid;
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return acid;
    }
    
    public static ArrayList<String> getCashTransactions() throws ClassNotFoundException, SQLException{
        ctamt.clear();
        ArrayList<String> tran = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from customtransfers where type = ?");
            stmt.setString(1, "Cash");
            rs = stmt.executeQuery();
            rs.first();
            tran.add(rs.getString("ToAccount"));
            ctamt.add(rs.getInt("Amount"));
            while (rs.next()) {                 
                tran.add(rs.getString("ToAccount"));
                ctamt.add(rs.getInt("Amount"));
            }
        } catch (SQLException e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return tran;
    }
    
    public static ArrayList<Integer> getCTAmount(){
        return ctamt;
    }
    
    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
}
