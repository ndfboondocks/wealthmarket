/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author Stephen
 */
public class Account_Settings {

    public Account_Settings() {

    }

    public static String setOverdraft(int od, String dat, String desc, String nam, String agid) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String result = "done";
        int acid = 0;
        int id = RandomNumber(9999, 100);

        /*Check if the agent account contains sufficient overdraft balance for the transaction*/
        String valid = CheckAgentOD(od, agid);
        /*If the agent overdraft balance is insufficient*/
        if (valid.equals("false")) {
            result = "denied";
            return result;
        } /* if the agent overdraft balance is sufficient for the transaction*/ else {
            try {
                con = new JDBCConnector().getConnection();
                /* Query the database for the Id of the user account*/
                stmt = con.prepareStatement("Select * from accounts where owner_name = ?");
                stmt.setString(1, nam);
                rs = stmt.executeQuery();
                rs.first();
                acid = rs.getInt("id");

                /*Record the overdraft event*/
                stmt = con.prepareStatement("INSERT INTO cyclos3.overdraft_history (id, overdraft_amount, expiry_date, account_id, agent_id) VALUES (?, ?, ?, ?, ?)");
                stmt.setInt(1, id);
                stmt.setInt(2, od);
                stmt.setString(3, dat);
                stmt.setInt(4, acid);
                stmt.setInt(5, 2);
                stmt.executeUpdate();

                /*Update the overdraft balance of the user*/
                UpdateOverdraft(acid, od);
            } catch (SQLException e) {
                result = "denied";
            } finally {
                rs.close();
                stmt.close();
                con.close();
            }
        }
        return result;
    }

    public static void UpdateOverdraft(int acid, int od) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = RandomNumber(9999, 100);
        String bdy = "An Overdraft line of =N" + od + " has been set on your account";

        try {
            con = new JDBCConnector().getConnection();
            /* Query the database for the overdraft details of the user account*/
            stmt = con.prepareStatement("Select * from overdraft where account_id = ?");
            stmt.setInt(1, acid);
            rs = stmt.executeQuery();
            /* If the user already has an overdraft input*/
            if (rs.first()) {
                od += rs.getInt("Overdraft_line");
                stmt = con.prepareStatement("Update overdraft set Overdraft_line = ? where account_id = ?");
                stmt.setInt(1, od);
                stmt.setInt(2, acid);
                stmt.executeUpdate();
            }/*if the user doesn't already have an overdraft input*/ else {
                stmt = con.prepareStatement("INSERT INTO cyclos3.overdraft (id, `Overdraft_line`, `Overdrawn`, account_id) VALUES (?, ?, ?, ?)");
                stmt.setInt(1, id);
                stmt.setInt(2, od);
                stmt.setInt(3, 0);
                stmt.setInt(4, acid);
                stmt.executeUpdate();
            }
            /* Query the database for the user id of the user*/
            stmt = con.prepareStatement("Select * from accounts where id = ?");
            stmt.setInt(1, acid);
            rs = stmt.executeQuery();
            rs.first();
            int itd = rs.getInt("member_id");
            /*Send confirmation message to the user*/
            sendMemberMessage(itd, bdy, "Overdraft");
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }

    public static String CheckAgentOD(int od, String agid) throws ClassNotFoundException, SQLException {
        String res = "false";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = RandomNumber(9999, 100);
        int bal = 0;

        try {
            con = new JDBCConnector().getConnection();
            /* Query the database for the agents account details*/
            stmt = con.prepareStatement("Select * from agents where agent_Id = ?");
            stmt.setString(1, agid);
            rs = stmt.executeQuery();
            rs.first();
            /*Get the overdraft balance of the agent account*/
            bal = rs.getInt("od_balance");
            /* if the agent overdraft balance is not sufficient for the transaction*/
            if (bal < od) {
                res = "false";
                return res;
            } /* If the agent overdraft balance is sufficient for the transaction*/ else {
                int all = bal - od;
                /*Update the overdraft balance of the agent*/
                stmt = con.prepareStatement("Update agents set od_balance = ? where agent_Id = ?");
                stmt.setInt(1, all);
                stmt.setString(2, agid);
                stmt.executeUpdate();
                res = "true";
                return res;
            }
        } catch (SQLException e) {
            res = "false";
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return res;
    }

    public static String addAgent(int id, String group) throws SQLException, ClassNotFoundException {
        String result = "success";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String type = "Agent";
        String pass = "";
        String name = "";
        int id1 = RandomNumber(99, 11);
        int id2 = RandomNumber(99, 11);
        String agid = "F" + id + id1;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            name = rs.getString("name");

            stmt = con.prepareStatement("Select * from users where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            pass = rs.getString("password");

            stmt = con.prepareStatement("INSERT INTO cyclos3.agents (`Id`, agent_name, agent_type, od_balance, password, `agent_Id`) VALUES (?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setString(3, type);
            stmt.setInt(4, 2000000);
            stmt.setString(5, pass);
            stmt.setString(6, agid);
            stmt.executeUpdate();

            changeUserType(id);
            String msg = "Congratulations " + name + ", Your request to become an agent of the Wealth Market has been granted. Please log In again and select your admin account to get started, Enjoy!";
            sendMemberMessage(id, msg, "Agent Request");
            changePermissionGroup(id, group);
        } catch (SQLException e) {
            result = "denied";
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }

    public static String changePassword(int id, String pass) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String res = "success";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Update users set password = ? where id = ?");
            stmt.setString(1, pass);
            stmt.setInt(2, id);
            stmt.executeUpdate();
            return res;
        } catch (SQLException e) {
            res = e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return res;
    }
    
    public static String changePermissionGroup(int id, String name) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String res = "";
        int gid = 0;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from user_groups where name = ?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            rs.first();
            gid = rs.getInt("id");
            
            stmt = con.prepareStatement("Update users set group_id = ? where id = ?");
            stmt.setInt(1, gid);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            res = e.getMessage();
        } finally {
            stmt.close();
            con.close();
        }
        return res;
    }

    public static void changeUserType(int id) throws SQLException, ClassNotFoundException {
        Connection con = null;
        PreparedStatement stmt = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Update members set subclass = ? where id = ?");
            stmt.setString(1, "F");
            stmt.setInt(2, id);
            stmt.executeUpdate();

            stmt = con.prepareStatement("Update users set subclass = ? where id = ?");
            stmt.setString(1, "F");
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            stmt.close();
            con.close();
        }
    }

    public static void sendMemberMessage(int id, String bdy, String sub) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        Date date = new Date();
        java.sql.Date dat = new java.sql.Date(date.getTime());

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.messages (`date`, subject, `type`, direction, is_read, is_replied, is_html, removed_at, from_member_id, to_member_id, category_id, body, email_sent) VALUES (CURRENT_TIMESTAMP, ?, 'msg', 'I', true, false, false, CURRENT_TIMESTAMP, NULL, ?, NULL, ?, true)");
            stmt.setString(1, sub);
            stmt.setInt(2, id);
            stmt.setString(3, bdy);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            stmt.close();
            con.close();
        }
    }
    
    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
}
