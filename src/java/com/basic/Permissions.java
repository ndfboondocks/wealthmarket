/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Stephen
 */
public class Permissions {
    
    public Permissions() {
        
    }
    
    public static HashMap<Integer, String> getPermissions() throws SQLException {
        HashMap<Integer, String> projname = new HashMap<Integer, String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from permissions");
            rs = stmt.executeQuery();
            rs.first();
            projname.put(rs.getInt("id"), rs.getString("name"));
            while (rs.next()) {
                projname.put(rs.getInt("id"), rs.getString("name"));
            }
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }
    
    public static HashMap<Integer, String> getCategories() throws SQLException {
        HashMap<Integer, String> projname = new HashMap<Integer, String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from permissions");
            rs = stmt.executeQuery();
            rs.first();
            projname.put(rs.getInt("id"), rs.getString("category"));
            while (rs.next()) {
                projname.put(rs.getInt("id"), rs.getString("category"));
            }
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }
    
    public static ArrayList<String> getCategoriesList() throws SQLException {
        ArrayList<String> projname = new ArrayList<String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from permissions");
            rs = stmt.executeQuery();
            rs.first();
            projname.add(rs.getString("category"));
            while (rs.next()) {
                projname.add(rs.getString("category"));
            }
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        ArrayList<String> nonDupList = new ArrayList<String>();
        
        Iterator<String> dupIter = projname.iterator();
        while (dupIter.hasNext()) {
            String dupWord = dupIter.next();
            if (nonDupList.contains(dupWord)) {
                dupIter.remove();
            } else {
                nonDupList.add(dupWord);
            }
        }
        
        return nonDupList;
    }
    
    public static int getUserGroupID(int id) throws SQLException {
        int grup = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from users where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            grup = rs.getInt("group_id");
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return grup;
    }
    
    public static HashMap<Integer, String> getUserPermissionsList(int id) throws SQLException {
        HashMap<Integer, String> list = new HashMap<Integer, String>();
        ArrayList<Integer> projname = new ArrayList<Integer>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from user_groups where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            projname.add(rs.getInt("permission_id"));
            while (rs.next()) {
                projname.add(rs.getInt("permission_id"));
            }
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        for (int i = 0; i < projname.size(); i++) {
            list.putAll(getUserPermissions(projname.get(i)));
        }
        return list;
    }
    
    public static HashMap<Integer, String> getUserPermissions(int id) throws SQLException {
        HashMap<Integer, String> projname = new HashMap<Integer, String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from permissions where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            projname.put(rs.getInt("id"), rs.getString("name"));
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }
    
    public static HashMap<Integer, String> getPermissionGroups() throws SQLException {
        HashMap<Integer, String> projname = new HashMap<Integer, String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from user_groups where name = ?");
            stmt.setString(1, "System Admin");
            rs = stmt.executeQuery();
            rs.first();
            projname.put(rs.getInt("permission_id"), rs.getString("name"));
            while (rs.next()) {
                projname.put(rs.getInt("permission_id"), rs.getString("name"));
            }
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }
    
    public static int getStaffPermissionID(int usid) throws SQLException {
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from staff where id = ?");
            stmt.setInt(1, usid);
            rs = stmt.executeQuery();
            rs.first();
            id = rs.getInt("sub_permission_id");
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return id;
    }
    
    public static ArrayList<Integer> getStaffPermissionIDList(int id, int agid) throws SQLException {
        ArrayList<Integer> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            con = new JDBCConnector().getConnection();
            if (id == 1) {
                stmt = con.prepareStatement("Select * from user_groups where id = ?");
                stmt.setInt(1, agid);
                rs = stmt.executeQuery();
                rs.first();
                list.add(rs.getInt("permission_id"));
                while (rs.next()) {                    
                    list.add(rs.getInt("permission_id"));
                }
            } else {
                stmt = con.prepareStatement("Select * from permission_groups where id = ?");
                stmt.setInt(1, id);
                rs = stmt.executeQuery();
                rs.first();
                list.add(rs.getInt("permission_id"));
                while (rs.next()) {                    
                    list.add(rs.getInt("permission_id"));
                }
            }
        } catch (Exception e) {
            
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return list;
    }
    
    public static String addPermGroup(int id, String name, int pid) throws SQLException {
        String result = "";
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO user_groups (id, `name`, permission_id) VALUES (?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, pid);
            stmt.executeUpdate();
        } catch (Exception e) {
            
        } finally {
            stmt.close();
            con.close();
        }
        return result;
    }
    
    public static String addAgentPermGroup(int id, String name, int pid, String agid) throws SQLException {
        String result = "";
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO permission_groups (id, `name`, permission_id, agent_id) VALUES (?, ?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, pid);
            stmt.setString(4, agid);
            stmt.executeUpdate();
        } catch (Exception e) {
            result = e.getMessage();
        } finally {
            stmt.close();
            con.close();
        }
        return result;
    }
    
    public static ArrayList<String> getAgentGroupList(String agid) throws SQLException {
        ArrayList<String> projname = new ArrayList<String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String res = "";
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from permission_groups where agent_id = ?");
            stmt.setString(1, agid);
            rs = stmt.executeQuery();
            rs.first();
            
            projname.add(rs.getString("name"));
            while (rs.next()) {
                projname.add(rs.getString("name"));
            }
        } catch (Exception e) {
            res = e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        ArrayList<String> nonDupList = new ArrayList<String>();
        
        Iterator<String> dupIter = projname.iterator();
        while (dupIter.hasNext()) {
            String dupWord = dupIter.next();
            if (nonDupList.contains(dupWord)) {
                dupIter.remove();
            } else {
                nonDupList.add(dupWord);
            }
        }
        
        return nonDupList;
    }
    
    public static ArrayList<String> getUserGroupList() throws SQLException {
        ArrayList<String> projname = new ArrayList<String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String res = "";
        
        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from user_groups");
            rs = stmt.executeQuery();
            rs.first();
            projname.add(rs.getString("name"));
            while (rs.next()) {
                projname.add(rs.getString("name"));
            }
        } catch (Exception e) {
            res = e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        ArrayList<String> nonDupList = new ArrayList<String>();
        
        Iterator<String> dupIter = projname.iterator();
        while (dupIter.hasNext()) {
            String dupWord = dupIter.next();
            if (nonDupList.contains(dupWord)) {
                dupIter.remove();
            } else {
                nonDupList.add(dupWord);
            }
        }
        
        return nonDupList;
    }
}
