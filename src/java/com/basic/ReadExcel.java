/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Stephen
 */
public class ReadExcel {

    public ReadExcel() {

    }

    public static String readExcel(String type, String state) {
        String res = "";
        String address = "c:\\" + type + ".xlsx";

        try {
            FileInputStream file = new FileInputStream(new File(address));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "t");
                            res += " " + cell.getNumericCellValue();
                            if (type.equals("States")) {
                                insertStates("" + cell.getNumericCellValue());
                            } else if (type.equals("Towns")) {
                                res = insertTowns("" + cell.getNumericCellValue(), state);
                            } else if (type.equals("Districts")) {
                                res = insertDistricts("" + cell.getNumericCellValue(), state);
                            } else {
                            }
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + "t");
                            res += " " + cell.getStringCellValue();
                            if (type.equals("States")) {
                                insertStates("" + cell.getStringCellValue());
                            } else if (type.equals("Towns")) {
                                res = insertTowns("" + cell.getStringCellValue(), state);
                            } else if (type.equals("Districts")) {
                                res = insertDistricts("" + cell.getStringCellValue(), state);
                            } else {
                            }
                            break;
                    }
                }
                res += " ";
                System.out.println("");
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
        return res;
    }

    public static void insertStates(String name) throws SQLException, ClassNotFoundException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        int id = RandomNumber(9999, 1111);

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.address_states (id, `name`, country) VALUES (?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setString(3, "Nigeria");
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            con.close();
        }
    }

    public static String insertTowns(String name, String state) throws SQLException, ClassNotFoundException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        int id = RandomNumber(9999, 1111);
        int ids = 0;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select id, name, country from address_states where name = ?");
            stmt.setString(1, state);
            rs = stmt.executeQuery();
            rs.first();
            ids = rs.getInt(1);

            stmt = con.prepareStatement("INSERT INTO cyclos3.address_towns (id, `name`, state_id) VALUES (?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, ids);
            stmt.executeUpdate();
        } catch (SQLException e) {
            return "" + e.getMessage();
        } finally {
            con.close();
        }
        return name;
    }

    public static String insertDistricts(String name, String town) throws SQLException, ClassNotFoundException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        int id = RandomNumber(9999, 1111);
        int ids = 0;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select id, name, state_id from address_towns where name = ?");
            stmt.setString(1, town);
            rs = stmt.executeQuery();
            rs.first();
            ids = rs.getInt(1);

            stmt = con.prepareStatement("INSERT INTO cyclos3.address_districts (id, `name`, town_id) VALUES (?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, ids);
            stmt.executeUpdate();
        } catch (SQLException e) {
            return "" + e.getMessage();
        } finally {
            con.close();
        }
        return name;
    }

    public static ArrayList<String> getStates() throws ClassNotFoundException, SQLException {
        ArrayList<String> states = new ArrayList<String>();
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        String problem = "";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("select * from address_states");
            rs = stmt.executeQuery();
            rs.first();
            int id = rs.getInt(1);
            String st = rs.getString(2);
            states.add(rs.getString(2));
            while (rs.next()) {
                states.add(rs.getString(2));
            }
        } catch (SQLException e) {
            problem = "" + e.getMessage();
        } finally {
            con.close();
        }
        return states;
    }

    public static ArrayList<String> getTowns(String state) throws ClassNotFoundException, SQLException {
        ArrayList<String> states = new ArrayList<String>();
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        int id = 0;
        String problem = "";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("select * from address_states where name = ?");
            stmt.setString(1, state);
            rs = stmt.executeQuery();
            rs.first();
            id = rs.getInt("id");

            stmt = con.prepareStatement("select * from address_towns where state_id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            states.add(rs.getString("name"));
            while (rs.next()) {
                states.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            problem = "" + e.getMessage();
        } finally {
            con.close();
        }
        return states;
    }

    public static HashMap<String, String> find(String child, String par) throws ClassNotFoundException, SQLException {
        HashMap<String, String> states = new HashMap<String, String>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = 0;
        String problem = "";

        try {
                con = new JDBCConnector().getConnection();
            if (child.equals("dd2")) {
                stmt = con.prepareStatement("select * from address_states where name = ?");
                stmt.setString(1, par);
                rs = stmt.executeQuery();
                rs.first();
                id = rs.getInt("id");

                stmt = con.prepareStatement("select * from address_towns where state_id = ?");
                stmt.setInt(1, id);
                rs = stmt.executeQuery();
                rs.first();
                states.put("" + rs.getInt("id"), rs.getString("name"));
                while (rs.next()) {
                    states.put("" + rs.getInt("id"), rs.getString("name"));
                }
            } else if(child.equals("dd3")){
                stmt = con.prepareStatement("select * from address_districts where town_id = ?");
                stmt.setInt(1, Integer.parseInt(par));
                rs = stmt.executeQuery();
                rs.first();
                states.put("" + rs.getInt("id"), rs.getString("name"));
                while (rs.next()) {
                    states.put("" + rs.getInt("id"), rs.getString("name"));
                }
            } else {
                stmt = con.prepareStatement("select * from address_streets where town_id = ?");
                stmt.setInt(1, Integer.parseInt(par));
                rs = stmt.executeQuery();
                rs.first();
                states.put("" + rs.getInt("id"), rs.getString("name"));
                while (rs.next()) {
                    states.put("" + rs.getInt("id"), rs.getString("name"));
                }
            }

        } catch (SQLException e) {
            problem = "" + e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return states;
    }

    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
}
