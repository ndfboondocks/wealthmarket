/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stephen
 */
public class ProjectServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            ArrayList<String> names = new ArrayList<>();
            int parid;

            String check = request.getParameter("addhide");
            String check2 = request.getParameter("btnget");
            String check3 = request.getParameter("subid");
            String check4 = request.getParameter("btnsub");
            String check5 = request.getParameter("btnBud");
            if (check != null) {
                String name = request.getParameter("name");
                String cat = request.getParameter("cat");
                String bud = request.getParameter("bud");
                int budget = Integer.parseInt(bud);
                String desc = request.getParameter("desc");

                String str = request.getParameter("start");
                String str2 = request.getParameter("fin");

                String state = request.getParameter("state");
                String lga = request.getParameter("lga");
                String loc = state + ", " + lga;

                Projects pr = new Projects();
                String res = "";
                if (check.equals("add")) {
                    res = pr.addProject(name, budget, loc, str, str2, desc, "M");
                } else if (check.equals("edit")) {
                    String sub = request.getParameter("sub");
                    int subscribed = Integer.parseInt(sub);
                    String ids = request.getParameter("id");
                    int id = Integer.parseInt(ids);
                    String ed = request.getParameter("adpro");
                    if (ed.equals("Save")) {
                        res = pr.updateProject(name, budget, loc, str, str2, desc, subscribed, id);
                    } else if (ed.equals("Delete")) {
                        res = pr.deleteProject(id);
                    }
                }
                names.clear();
                names = pr.getProjectNames();
                HttpSession nms = request.getSession(true);
                nms.setAttribute("type", "one");
                nms.setAttribute("names", names);
                if (res.equals("accepted")) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("Project added Successfully!!!");
                    out.println("location='pages/Agents/agentProject.jsp';");
                    out.println("</script>");
                    response.sendRedirect("pages/Agents/agentProject.jsp");
                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("Something went wrong, Try again Later!!!");
                    out.println("location='pages/Agents/agentProject.jsp';");
                    out.println("</script>");
                    response.sendRedirect("pages/Agents/agentProject.jsp");
                }
            } else if (check2 != null) {
                ArrayList<String> pro = new ArrayList<>();
                ArrayList<String> subs = new ArrayList<>();
                Projects pr = new Projects();
                pro = pr.getProjectDetails(check2);
                subs = pr.getSubProjects(check2);
                parid = Integer.parseInt(pro.get(7));
                HttpSession nms = request.getSession(true);
                nms.setAttribute("type", "two");
                nms.setAttribute("details", pro);
                nms.setAttribute("subs", subs);
                nms.setAttribute("par", parid);
                response.sendRedirect("pages/Agents/agentProject.jsp");
            } else if (check3 != null) {
                HttpSession nms = request.getSession(true);
                ArrayList<String> subs = new ArrayList<>();
                String name = request.getParameter("name");
                String bud = request.getParameter("bud");
                int budget = Integer.parseInt(bud);
                String parent = request.getParameter("sub");
                int par = Integer.parseInt(parent);
                String desc = request.getParameter("desc");
                Projects pr = new Projects();
                pr.addSubProject(name, desc, budget, par);
                subs = pr.getSubProjects(check2);
                nms.setAttribute("subs", subs);
                nms.setAttribute("type", "one");
                response.sendRedirect("pages/Agents/agentProject.jsp");
            } else if (check4 != null) {
                ArrayList<String> pro = new ArrayList<>();
                ArrayList<String> subs = new ArrayList<>();
                Projects pr = new Projects();
                pro = pr.getProjectDetails(check4);
                subs = pr.getSubProjects(check4);
                parid = Integer.parseInt(pro.get(7));
                HttpSession nms = request.getSession(true);
                nms.setAttribute("type", "two");
                nms.setAttribute("details", pro);
                nms.setAttribute("subs", subs);
                nms.setAttribute("par", parid);
                response.sendRedirect("pages/Agents/agentProject.jsp");
            } else if (check5 != null) {
                String name = request.getParameter("comp");
                String quo = request.getParameter("quo");
                int quantity = Integer.parseInt(quo);
                String rat = request.getParameter("rat");
                int rate = Integer.parseInt(rat);
                String unit = request.getParameter("unit");
                String par = request.getParameter("parid");
                int proId = Integer.parseInt(par);
                
                Projects pr = new Projects();
                pr.insertBudget(name, quantity, rate, proId, unit);
                response.sendRedirect("pages/Agents/agentProject.jsp");
            } else {
                names.clear();
                Projects pr = new Projects();
                names = pr.getProjectNames();
                HttpSession nms = request.getSession(true);
                nms.setAttribute("names", names);
                nms.setAttribute("type", "one");
                response.sendRedirect("pages/Agents/agentProject.jsp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ProjectServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ProjectServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(ProjectServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ProjectServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
