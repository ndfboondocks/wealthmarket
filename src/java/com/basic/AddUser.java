/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.Date;

/**
 *
 * @author Stephen
 */
public class AddUser {

    static int Meid;

    public AddUser() {

    }

    public static String createMember(String nam, String ema, String unam, String pass, String add, String phone) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String result = null;
        String newadd = "ojo street";
        if (add.equals("Please select your town")|| add.equals("")) {
            add = newadd;
        } else {
            
        }

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members where email = ?");
            stmt.setString(1, ema);
            rs = stmt.executeQuery();
            rs.first();
            if (rs.first()) {
                result = "exists";
            } else {
                stmt = con.prepareStatement("INSERT INTO cyclos3.members (subclass, `name`, creation_date, group_id, email, member_activation_date, hide_email, member_broker_id, member_id) VALUES (?, ?, CURRENT_TIMESTAMP, ?, ?, CURRENT_TIMESTAMP, DEFAULT, NULL, NULL)");
                stmt.setString(1, "M");
                stmt.setString(2, nam);
                stmt.setInt(3, 5);
                stmt.setString(4, ema);
                stmt.executeUpdate();

                stmt = con.prepareStatement("Select * from members where email = ?");
                stmt.setString(1, ema);
                rs = stmt.executeQuery();
                rs.first();
                Meid = rs.getInt("id");
                
                stmt = con.prepareStatement("Select * from address_streets where name = ?");
                stmt.setString(1, add);
                rs = stmt.executeQuery();
                rs.first();
                int dist = rs.getInt("id");
                con.close();
                result = InsertUser(Meid, unam, pass, dist, phone);
                createAccounts(nam, Meid);
            }
        } catch (SQLException e) {
            result = "invalid";
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }

    public static void createAccounts(String nam, int mid) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int acno = RandomNumber(999999, 100000);
        String bdy = "A current account has been created for you. Your account number is " + acno + " .Welcome to the Wealth Market!!";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO accounts (subclass, creation_date, last_closing_date, owner_name, type_id, credit_limit, upper_credit_limit, member_id, member_status, last_low_units_sent, member_action, account_number) VALUES (?, CURRENT_TIMESTAMP, CURRENT_DATE, ?, 5, NULL, NULL, ?, NULL, CURRENT_TIMESTAMP, NULL, ?)");
            stmt.setString(1, "M");
            stmt.setString(2, nam);
            stmt.setInt(3, mid);
            stmt.setInt(4, acno);
            stmt.executeUpdate();
            createBalances(mid);
            sendMemberMessage(mid, bdy, "Account Created");
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }

    public static void createBalances(int mid) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from accounts where member_id = ?");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            rs.first();
            int rid = rs.getInt("id");

            stmt = con.prepareStatement("INSERT INTO account_fee_amounts (account_id, `date`, available_balance, amount, account_fee_id, blocked) VALUES (?, CURRENT_DATE, 1000, 100, 1, 0)");
            stmt.setInt(1, rid);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }

    public static String InsertUser(int meid, String unam, String pass, int dis, String phone) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int rp1 = RandomNumber(999, 100);
        int rp = RandomNumber(99, 01);
        String tpass = rp1 + "AB" + rp;
        String bdy = "Congratulations!!! You have been successfully registered as a member of the Wealth Market."
                + "Now take a tour and make use of out many, many, many, many and seriously many more functionalities that will enrich you life..."
                + "Happy Transacting!!!.... or... whatever you're gonna be doing!";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("insert into users (id, subclass, username, password, transaction_password, transaction_password_status, phone_number, group_id, street_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, meid);
            stmt.setString(2, "M");
            stmt.setString(3, unam);
            stmt.setString(4, pass);
            stmt.setString(5, tpass);
            stmt.setString(6, "N");
            stmt.setString(7, phone);
            stmt.setInt(8, 1);
            stmt.setInt(9, dis);
            stmt.executeUpdate();

            sendMemberMessage(meid, bdy, "Welcome to the Wealth Market");
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return tpass;
    }

    private static SecureRandom rand = new SecureRandom();

    public static String nextSessionId() {
        return new BigInteger(130, rand).toString(32);
    }

    public static String AddStaff(String nam, String agid, String perm) throws ClassNotFoundException, SQLException {
        String result = "failed";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int rp1 = RandomNumber(99999, 10000);
        int rp = RandomNumber(9999, 1000);
        int id = 0;
        int permid = 0;
        String pass = "";

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from members where name = ?");
            stmt.setString(1, nam);
            rs = stmt.executeQuery();
            rs.first();
            id = rs.getInt("id");

            if (perm.equals("General")) {
                permid = 1;
            } else {
                stmt = con.prepareStatement("Select * from permission_groups where agent_id = ? and name = ?");
                stmt.setString(1, agid);
                stmt.setString(2, perm);
                rs = stmt.executeQuery();
                rs.first();
                permid = rs.getInt("id");
            }

            stmt = con.prepareStatement("Select * from users where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            pass = rs.getString("password");

            stmt = con.prepareStatement("INSERT INTO cyclos3.staff (`Id`, staff_name, `staff_Id`, `agent_Id`, sub_permission_id, password) values (?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, nam);
            stmt.setInt(3, rp);
            stmt.setString(4, agid);
            stmt.setInt(5, permid);
            stmt.setString(6, pass);
            stmt.executeUpdate();

            String bdy = "You have been successfully registered as a staff of an agency. Please log In again and select your admin account to get started";
            changeMemberClass(nam, id);
            sendMemberMessage(id, bdy, "Agency Registration");
            result = "successful";
        } catch (SQLException e) {
            result = e.getMessage();
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }

    public static void changeMemberClass(String name, int id) throws SQLException, ClassNotFoundException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Update members set subclass = ? where name = ?");
            stmt.setString(1, "S");
            stmt.setString(2, name);
            stmt.executeUpdate();

            stmt = con.prepareStatement("Update users set subclass = ? where id = ?");
            stmt.setString(1, "S");
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }

    public static void sendMemberMessage(int id, String bdy, String sub) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Date date = new Date();
        java.sql.Date dat = new java.sql.Date(date.getTime());

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.messages (`date`, subject, `type`, direction, is_read, is_replied, is_html, removed_at, from_member_id, to_member_id, category_id, body, email_sent) VALUES (CURRENT_TIMESTAMP, ?, 'msg', 'I', true, false, false, CURRENT_TIMESTAMP, NULL, ?, NULL, ?, true)");
            stmt.setString(1, sub);
            stmt.setInt(2, id);
            stmt.setString(3, bdy);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
    }
        
    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
}
