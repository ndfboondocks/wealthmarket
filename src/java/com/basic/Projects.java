/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import static com.basic.Accounting_Entry.name;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Stephen
 */
public class Projects {

    public Projects() {

    }

    public static ArrayList<String> getProjectNames() throws SQLException {
        ArrayList<String> projname = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from projects where type = ?");
            stmt.setString(1, "M");
            rs = stmt.executeQuery();
            rs.first();
            projname.add(rs.getString("Name"));
            while (rs.next()) {
                projname.add(rs.getString("Name"));
            }
        } catch (Exception e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }

    public static ArrayList<String> getProjectDetails(String name) throws SQLException {
        ArrayList<String> projname = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from projects where Name = ?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            rs.first();
            projname.add(rs.getString("Name"));
            projname.add(rs.getString("description"));
            projname.add(rs.getString("location"));
            projname.add("" + rs.getInt("budget"));
            projname.add("" + rs.getInt("subscribed"));
            projname.add("" + rs.getDate("start"));
            projname.add("" + rs.getDate("finished"));
            projname.add("" + rs.getInt("id"));
            while (rs.next()) {
                projname.add(rs.getString("Name"));
                projname.add(rs.getString("description"));
                projname.add(rs.getString("location"));
                projname.add("" + rs.getInt("budget"));
                projname.add("" + rs.getInt("subscribed"));
                projname.add("" + rs.getDate("start"));
                projname.add("" + rs.getDate("finished"));
                projname.add("" + rs.getInt("id"));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }

    public static String addProject(String name, int budget, String loc, String str, String fin, String desc, String type) throws SQLException {
        String result = "accepted";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = RandomNumber(9999, 1111);
        Date start = Date.valueOf(str);
        Date finish = Date.valueOf(fin);

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.projects (id, `Name`, budget, location, `start`, finished, description, subscribed, type) VALUES (?, ?, ?, ?, ?, ?, ?, 0, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, budget);
            stmt.setString(4, loc);
            stmt.setDate(5, start);
            stmt.setDate(6, finish);
            stmt.setString(7, desc);
            stmt.setString(8, type);
            stmt.executeUpdate();
            return result;
        } catch (Exception e) {
            result = "denied";
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }

    public static String updateProject(String name, int budget, String loc, String str, String fin, String desc, int sub, int id) throws SQLException {
        String result = "accepted";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Date start = Date.valueOf(str);
        Date finish = Date.valueOf(fin);

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Update projects set id = ?, `Name` = ?, budget = ?, location = ?, `start` = ?, finished = ?, description = ?, subscribed = ? where id = ?");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, budget);
            stmt.setString(4, loc);
            stmt.setDate(5, start);
            stmt.setDate(6, finish);
            stmt.setString(7, desc);
            stmt.setInt(8, sub);
            stmt.setInt(9, id);
            stmt.executeUpdate();
            return result;
        } catch (Exception e) {
            result = "denied";
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }
    
    public static String deleteProject(int id) throws SQLException {
        String result = "accepted";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Delete from projects where id = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            return result;
        } catch (Exception e) {
            result = "denied";
        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }
    
    public static ArrayList<String> getSubProjects(String name) throws SQLException {
        ArrayList<String> projname = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from projects where Name = ?");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            rs.first();
            int id = rs.getInt("id");
            
            stmt = con.prepareStatement("Select * from sub_projects where parent_id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            projname.add(rs.getString("name"));
            while (rs.next()) {
                projname.add(rs.getString("name"));
            }
        } catch (Exception e) {

        }finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return projname;
    }
    
    public static String addSubProject(String name, String desc, int budget, int parid) throws SQLException{
        String result = "";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int id = RandomNumber(9999, 1111);

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from projects where id = ?");
            stmt.setInt(1, parid);
            rs = stmt.executeQuery();
            rs.first();
            String loc = rs.getString("location");
            String str = ""+rs.getDate("start");
            String fin = ""+rs.getDate("finished");
            
            stmt = con.prepareStatement("INSERT INTO cyclos3.sub_projects (id, parent_id, `name`, description, budget, subscribed) VALUES (?, ?, ?, ?, ?, 0)");
            stmt.setInt(1, id);
            stmt.setInt(2, parid);
            stmt.setString(3, name);
            stmt.setString(4, desc);
            stmt.setInt(5, budget);
            stmt.executeUpdate();
            
            addProject(name, budget, loc, str, fin, desc, "S");
        } catch (Exception e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }
    
    public static String insertBudget(String comp, int quo, int rate, int par, String unit) throws SQLException{
        String result = "successful";
        int id = RandomNumber(9999, 1111);
        int total = quo * rate;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.budget_components (id, `name`, quantity, unit, total, project_id, unit_measure) VALUES (?, ?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, id);
            stmt.setString(2, comp);
            stmt.setInt(3, quo);
            stmt.setInt(4, rate);
            stmt.setInt(5, total);
            stmt.setInt(6, par);
            stmt.setString(7, unit);
            stmt.executeUpdate();
        } catch (Exception e) {

        } finally {
            rs.close();
            stmt.close();
            con.close();
        }
        return result;
    }
    
    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
}
