/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Stephen
 */
public class CreateUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String ans;
            String type = request.getParameter("type");
            String fnam = request.getParameter("fnam");
            String lnam = request.getParameter("lnam");
            String nam = fnam + " " + lnam;
            String pass = request.getParameter("pass");
            String address = request.getParameter("dd4");
            String phone = request.getParameter("phone");
            String cpass = request.getParameter("cpass");
            String ema = request.getParameter("ema");
            String unam = request.getParameter("unam");
            
            if (pass.equals(cpass)) {
                ans = AddUser.createMember(nam, ema, unam, pass, address, phone);
                if (type.equals("one")) {
                    if (ans.equals("invalid")) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Something went wrong. Try again later');");
                        out.println("location='pages/Agents/agentHome.jsp';");
                        out.println("</script>");
                    } else if (ans.equals("exists")) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('The email has been registered to another User');");
                        out.println("location='pages/Agents/agentHome.jsp';");
                        out.println("</script>");
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('User Added Successfully!');");
                        out.println("location='pages/Agents/agentHome.jsp';");
                        out.println("</script>");
                    }
                } else {
                    if (ans.equals("invalid")) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Something went wrong. Try again later');");
                        out.println("location='createUser.jsp';");
                        out.println("</script>");
                    } else if (ans.equals("exists")) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('The email has been registered to another User');");
                        out.println("location='createUser.jsp';");
                        out.println("</script>");
                    } else {
                        ans = Login.getUsers(pass, ema);
                        HashMap<String, String> det = new HashMap<String, String>();
                        int id = Login.getMemID(ema);
                        det = Login.getAccountDet(id);
                        HttpSession session = request.getSession(true);
                        
                        session.setAttribute("sessionBal", det.get("balance"));
                        session.setAttribute("sessionName", det.get("name"));
                        session.setAttribute("blocked", det.get("blocked"));
                        session.setAttribute("available", det.get("available"));
                        session.setAttribute("sessionDate", det.get("created"));
                        session.setAttribute("Id", det.get("id"));
                        session.setAttribute("sessId", det.get("account_id"));
                        session.setAttribute("currentSessionUser", det.get("user"));
                        response.sendRedirect("pages/Members/home.jsp"); //logged-in page   	
                    }
                }
            } else {
                if (type.equals("one")) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Password doesnt match Confirmation');");
                    out.println("location='pages/Agents/agentHome.jsp';");
                    out.println("</script>");
                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Password doesnt match Confirmation');");
                    out.println("location='createUser.jsp';");
                    out.println("</script>");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(CreateUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
