/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author Stephen
 */
public class Accounting_Entry {

    static String profile;
    static String name;
    static float interest;
    static float directrate;
    static float collective = 0;
    static HashMap<String, Integer> matrix = new HashMap<String, Integer>();
    static HashMap<String, Integer> sub = new HashMap<String, Integer>();
    static ArrayList<Integer> sublist = new ArrayList<Integer>();
    static ArrayList<String> subnam = new ArrayList<String>();
    static ArrayList<Integer> genlist = new ArrayList<Integer>();
    static ArrayList<Float> temp = new ArrayList<Float>();
    static ArrayList<Float> reftemp = new ArrayList<Float>();
    static ArrayList<String> tempsec = new ArrayList<String>();
    static ArrayList<String> reftempsec = new ArrayList<String>();

    public Accounting_Entry() {

    }

    public static void getProfileDetails(String pro) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from profile where name = ?");
            stmt.setString(1, pro);
            rs = stmt.executeQuery();
            rs.first();
            interest = rs.getFloat("interest");
            profile = pro;

            getMansar();
        } catch (SQLException e) {

        } finally {
            con.close();
        }
    }

    public static void getMansar() throws ClassNotFoundException, SQLException {
        matrix.clear();
        sublist.clear();
        genlist.clear();
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        int curid = 0;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from current_matrix");
            rs = stmt.executeQuery();
            rs.last();
            curid = rs.getInt("matrix_id");

            stmt = con.prepareStatement("Select * from ledgers");
            rs = stmt.executeQuery();
            rs.first();
            sublist.add(rs.getInt("sub_ledger_id"));
            subnam.add(rs.getString("name"));
            genlist.add(rs.getInt("general_ledger_id"));
            while (rs.next()) {
                sublist.add(rs.getInt("sub_ledger_id"));
                subnam.add(rs.getString("name"));
                genlist.add(rs.getInt("general_ledger_id"));
            }

            stmt = con.prepareStatement("Select * from mansar_matrix where id = ?");
            stmt.setInt(1, curid);
            rs = stmt.executeQuery();
            rs.first();
            float dire = rs.getInt("ratio");
            collective += rs.getInt("ratio");
            while (rs.next()) {
                matrix.put(rs.getString("sector_name"), rs.getInt("ratio"));
                sub.put(rs.getString("sector_name"), rs.getInt("sub_ledger_id"));
                collective += rs.getInt("ratio");
            }
            directrate = dire / collective;
        } catch (SQLException e) {

        } finally {
            con.close();
        }
    }

    public static float getEntryDetails(int id, int amount) throws ClassNotFoundException, SQLException {
        getProfileDetails("200 for 67");
        float Reflation = (interest / 100) * amount;
        float totalWarrants = Reflation + amount;
        float direct = (directrate * Reflation);
        float currentDated = amount + direct;
        float rat = (interest / totalWarrants);
        float refCurrent = rat * currentDated;
        int trnId = Transfer(totalWarrants, id);
        insertProfileRecord(amount, trnId, name, profile);

        for (int i = 0; i < sublist.size(); i++) {
            temp.clear();
            tempsec.clear();
            reftemp.clear();
            reftempsec.clear();
            float subacc = 0; 
            float refsubacc = 0; 
            float add = 0;
            String sec = "";
            Set<String> keys = matrix.keySet();
            Iterator<String> Iterator = keys.iterator();
            while (Iterator.hasNext()) {
                String a = Iterator.next();
                float sum = matrix.get(a) / collective;
                add = (sum * Reflation);
                refsubacc += rat * add;
                if (sub.get(a).equals(sublist.get(i))) {
                    subacc += add;
                    sec = a;
                    tempsec.add(sec);
                    temp.add(add);
                    reftempsec.add("ref"+sec);
                    reftemp.add(refsubacc);
                }
            }
            int gen = genlist.get(i);
                int subid;             
                if (subnam.get(i).contains("Ref")) {
                    subid = insertLedger(refsubacc, gen, subnam.get(i));
                }
                else{
                    subid = insertLedger(subacc, gen, subnam.get(i));   
                }
            if (temp.isEmpty()) {}
            else{
                for (int j = 0; j < temp.size(); j++) {
                    insertPrimary(id, sublist.get(i), 0, temp.get(j), tempsec.get(j), trnId);
                    insertPrimary(id, sublist.get(i), reftemp.get(j), 0, reftempsec.get(j), trnId);
                }
            }
        }

        /*float refForward = rat * forwardDated;
        float refInsurance = rat * Insurance;
        float refInfraPro = rat * InfraPro;
        float refmktOp = rat * mktOp;
        float refMngDisc = rat * MngDisc;

        insertPrimary(id, subid4, refCurrent, 0, "Reflation Current Dated", trnId);
        insertPrimary(id, subid4, refForward, 0, "Reflation Forward Dated", trnId);
        insertPrimary(id, subid4, refInsurance, 0, "Reflation Insurance", trnId);
        insertPrimary(id, subid5, refInfraPro, 0, "Reflation Infrastructure Project", trnId);
        insertPrimary(id, subid5, refmktOp, 0, "Reflation Market Operators", trnId);
        insertPrimary(id, subid5, refMngDisc, 0, "Reflation Managers Discretion", trnId);*/
        return currentDated;
    }

    public static int insertLedger(float amount, int gen, String nam) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        int subid = RandomNumber(9999, 1000);

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String username = "root";
            String password = "sabbath10";
            String Database = "jdbc:mysql://localhost:3306/cyclos3";
            con = DriverManager.getConnection(Database, username, password);

            stmt = con.prepareStatement("Select * from ledgers where name = ?");
            stmt.setString(1, nam);
            rs = stmt.executeQuery();
            if (rs.first()) {
                float tempcash = rs.getFloat("amount");
                stmt = con.prepareStatement("Update ledgers set amount = ? where name = ?");
                stmt.setFloat(1, amount + tempcash);
                stmt.setString(2, nam);
                stmt.executeUpdate();
            } else {
                stmt = con.prepareStatement("INSERT INTO cyclos3.ledgers (sub_ledger_id, `name`, general_ledger_id, amount) VALUES (?, ?, ?, ?)");
                stmt.setInt(1, subid);
                stmt.setString(2, nam);
                stmt.setFloat(3, gen);
                stmt.setFloat(4, amount);
                stmt.executeUpdate();
            }

        } catch (SQLException e) {

        } finally {
            con.close();
        }
        return subid;
    }

    public static void insertPrimary(int id, int sub_id, float dbt, float crd, String name, int trnId) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String username = "root";
            String password = "sabbath10";
            String Database = "jdbc:mysql://localhost:3306/cyclos3";
            con = DriverManager.getConnection(Database, username, password);

            stmt = con.prepareStatement("INSERT INTO cyclos3.primary_accounts (member_id, sub_ledger_id, debit, credit, `Date`, `name`, `trnId`) VALUES (?, ?, ?, ?, CURRENT_DATE, ?, ?)");
            stmt.setInt(1, id);
            stmt.setInt(2, sub_id);
            stmt.setFloat(3, dbt);
            stmt.setFloat(4, crd);
            stmt.setString(5, name);
            stmt.setInt(6, trnId);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            con.close();
        }
    }

    public static int Transfer(float amt, int mid) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;
        String toac = "";
        Date date = new Date();
        java.sql.Date dt = new java.sql.Date(date.getTime());
        int id = RandomNumber(9999, 1000);

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String username = "root";
            String password = "sabbath10";
            String Database = "jdbc:mysql://localhost:3306/cyclos3";
            con = DriverManager.getConnection(Database, username, password);

            stmt = con.prepareStatement("Select * from accounts where id = ?");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            rs.first();
            toac = rs.getString("owner_name");
            int midi = rs.getInt("member_id");
            name = toac;

            stmt = con.prepareStatement("insert into customtransfers values(?, ?, ?, ?, ?, ?, ?)");
            stmt.setString(1, "Cash");
            stmt.setString(2, "Wealth Market");
            stmt.setString(3, toac);
            stmt.setDate(4, dt);
            stmt.setInt(5, (int) amt);
            stmt.setInt(6, id);
            stmt.setInt(7, midi);
            stmt.executeUpdate();

        } catch (SQLException e) {

        } finally {
            con.close();
        }
        return id;
    }

    public static void insertProfileRecord(int amount, int trnid, String name, String profile) throws ClassNotFoundException, SQLException {
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("INSERT INTO cyclos3.cash_profile_transactions (id, account_name, amount, profile, `date`) VALUES (?, ?, ?, ?, CURRENT_DATE)");
            stmt.setInt(1, trnid);
            stmt.setString(2, name);
            stmt.setInt(3, amount);
            stmt.setString(4, profile);
            stmt.executeUpdate();
        } catch (SQLException e) {

        } finally {
            con.close();
        }
    }

    public static ArrayList<Integer> getMatrixIDs() throws ClassNotFoundException, SQLException{
        ArrayList<Integer> ids = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from mansar_matrix");
            rs = stmt.executeQuery();
            rs.first();
            ids.add(rs.getInt("id"));
        } catch (SQLException e) {

        } finally {
            con.close();
        }
        return ids;
    }
    
    public static HashMap<String, Float> getMatrix(int id) throws ClassNotFoundException, SQLException{
        HashMap<String, Float> mat = new HashMap<>();
        ArrayList<String> sec = new ArrayList<>();
        ArrayList<Integer> rat = new ArrayList<>();
        int collect = 0;
        float n;
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from mansar_matrix where id = ?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.first();
            sec.add(rs.getString("sector_name"));
            rat.add(rs.getInt("ratio"));
            collect += rs.getInt("ratio");
            while (rs.next()) {         
                sec.add(rs.getString("sector_name"));
                rat.add(rs.getInt("ratio"));       
                collect += rs.getInt("ratio");
            }
        } catch (SQLException e) {

        } finally {
            con.close();
        }
        for (int i = 0; i < sec.size(); i++) {
            int a = rat.get(i);
            n = a * 100;
            float percent = n / collect;
            float perc = precision(1, percent);
            mat.put(sec.get(i), perc);
        }
        return mat;
    }
    
    public static int RandomNumber(int max, int min) {
        Random rand = new Random();
        int itID = rand.nextInt((max - min) + 1) + min;
        return itID;
    }
    
    public static Float precision(int decimalPlace, Float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
    
    public static ArrayList<String> getPrimaries() throws ClassNotFoundException, SQLException{
        ArrayList<String> ids = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt;
        ResultSet rs;

        try {
            con = new JDBCConnector().getConnection();
            stmt = con.prepareStatement("Select * from ledgers");
            rs = stmt.executeQuery();
            rs.first();
            ids.add(rs.getString("name"));
            while (rs.next()) {                
                ids.add(rs.getString("name"));
            }
        } catch (SQLException e) {

        } finally {
            con.close();
        }
        return ids;
    }
}
