/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.basic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stephen
 */
public class PermServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String type = request.getParameter("categ");
            if (type.equals("admin")) {
                String name = request.getParameter("name");
                String perms[] = request.getParameterValues("permissions");
                if (name.equals("")) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Please Specify a Group Name');");
                    out.println("location='pages/Agents/agentSettings.jsp';");
                    out.println("</script>");
                } else if (perms == null) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Please Select at least one permission');");
                    out.println("location='pages/Agents/agentSettings.jsp';");
                    out.println("</script>");
                } else {
                    int id = Login.RandomNumber(9999, 1111);
                    for (int i = 0; i < perms.length; i++) {
                        int pid = Integer.parseInt(perms[i]);
                        String res = Permissions.addPermGroup(id, name, pid);
                    }
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Group Added Successfully');");
                    out.println("location='pages/Agents/agentSettings.jsp';");
                    out.println("</script>");
                }
            } else if(type.equals("agent")){
                String name = request.getParameter("name");
                String agid = request.getParameter("agentid");
                String perms[] = request.getParameterValues("permissions");
                if (name.equals("")) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Please Specify a Group Name');");
                    out.println("location='pages/AgentAdmin/agentAdminSettings.jsp';");
                    out.println("</script>");
                } else if (perms == null) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Please Select at least one permission');");
                    out.println("location='pages/AgentAdmin/agentAdminSettings.jsp';");
                    out.println("</script>");
                } else {
                    int id = Login.RandomNumber(9999, 1111);
                    for (int i = 0; i < perms.length; i++) {
                        int pid = Integer.parseInt(perms[i]);
                        String res = Permissions.addAgentPermGroup(id, name, pid, agid);
                    }
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Group Added Successfully');");
                    out.println("location='pages/AgentAdmin/agentAdminSettings.jsp';");
                    out.println("</script>");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PermServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(PermServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
