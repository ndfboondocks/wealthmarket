<%-- 
    Document   : home
    Created on : 25-Nov-2014, 12:07:14
    Author     : Stephen
--%>

<%@page import="java.util.Date"%>
<%@page import="com.basic.Transfer"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>  

<%
    String as = "" + session.getAttribute("currentSessionUser");
    String x = request.getParameter("btnTrn");
    int bal = 0;
    if (x != null && x.equals("Transfer")) {
        String am = request.getParameter("amount");
        String ben = request.getParameter("ben");
        if (am != "" && ben != "") {
            String ans;
            int acno = Integer.parseInt(request.getParameter("ben"));
            String nam = "" + session.getAttribute("sessionName");
            String desc = request.getParameter("desc");
            /* Get the id of the current user*/
            int mid = Integer.parseInt("" + session.getAttribute("sessId"));
            /* Get the amount to transfer*/
            int amt = Integer.parseInt(request.getParameter("amount"));
            /* Get the available balance of the user*/
            int bala = Integer.parseInt(request.getParameter("balance"));
            /* Get the current date and time*/
            Date sdt = new Date();
            java.sql.Date dt = new java.sql.Date(sdt.getTime());
            /* After collecting all valuable information, make the transfer*/
            String nans = Transfer.InsertTransfer(bala, nam, acno, amt, dt, mid, desc);

            if (nans.equals("failed")) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Sorry something went wrong.... Try again later');");
                out.println("location='account.jsp';");
                out.println("</script>");
            } else if (nans.equals("insufficient")) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Sorry Your balance is too low for this transaction');");
                out.println("location='account.jsp';");
                out.println("</script>");
            } else if (nans.equals("invalid")) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Sorry the account doesn't exist);");
                out.println("location='account.jsp';");
                out.println("</script>");
            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Transfer was successful');");
                out.println("location='account.jsp';");
                out.println("</script>");
            }
        } else {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Wrong details');");
            out.println("location='account.jsp';");
            out.println("</script>");
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="../../scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="../../scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic-Transact</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../../WEB-INF/static_pages/header.jsp"></jsp:include>
                <div id="content">
                    <div id="profile"><%= as%> accounts</div>
                <div id="accountBody">
                    <div>
                        <div class="titles">Account Details</div>
                        <div>
                            <%
                                Connection con = null;
                                PreparedStatement stmt;
                                ResultSet rs;
                                String str = "" + session.getAttribute("sessId");
                                int id = Integer.parseInt(str);
                                try {
                                    Class.forName("com.mysql.jdbc.Driver");
                                    String username = "root";
                                    String password = "sabbath10";
                                    String Database = "jdbc:mysql://localhost:3306/cyclos3";
                                    con = DriverManager.getConnection(Database, username, password);

                                    stmt = con.prepareStatement("Select * from account_fee_amounts where account_id = ?");
                                    stmt.setInt(1, id);
                                    rs = stmt.executeQuery();
                                    rs.first();
                                    bal = rs.getInt("available_balance");
                                    int block = rs.getInt("blocked");
                            %>
                            <div id="accountDet">
                                <div id="pic" class="left"></div>
                                <div class="left" id="pros">
                                    <div class="row"><span class="init">Account Name:</span><span class="res"><%= session.getAttribute("sessionName")%></span></div>
                                    <div class="row"><span class="init">Account Type:</span> <span class="res">Current Account</span></div>
                                    <div class="row"><span class="init">Date Created:</span><span class="res"><%= session.getAttribute("sessionDate")%></span></div>
                                    <div class="row"><span class="init">Ledger Balance:</span><span class="res">=N <%= bal%></span></div>
                                    <div style="clear: both"></div>
                                </div>
                                <div class="left" id="pros">
                                    <div class="row"><span class="init">Amount Blocked:</span><span class="res">=N <%= session.getAttribute("blocked")%></span></div>
                                    <div class="row"><span class="init">Available Balance:</span><span class="res">=N <%= session.getAttribute("available")%></span></div>
                                    <div style="clear: both"></div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>
                        <div class="titles">Recent Transactions</div>
                        <div id="trans">
                            <table id ="tab">
                                <thead>
                                    <tr id="hed">
                                        <th>Date</th>
                                        <th>Balance before</th>
                                        <th>Time</th>
                                        <th>Trxn ID</th>
                                        <th>Other Account (Name)</th>
                                        <th>Other Account (Number)</th>
                                        <th>-Debit / Credit</th>
                                        <th>Balance after</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        stmt = con.prepareStatement("Select * from customtransfers where account_id = ?");
                                        stmt.setInt(1, id);
                                        rs = stmt.executeQuery();
                                        if (rs.first()) {
                                    %>
                                    <tr class="row2">
                                        <td><%= rs.getDate("Date")%></td>
                                        <td><%= rs.getInt("old_balance")%></td>
                                        <td><%= rs.getTime("Time")%></td>
                                        <td><%= rs.getInt("id")%></td>
                                        <td><%= rs.getString("OtherAccountName")%></td>
                                        <td><%= rs.getInt("OtherAccountNumber")%></td>
                                        <td><%= rs.getInt("Amount")%></td>
                                        <td><%= rs.getInt("new_balance")%></td>
                                    </tr>
                                    <%
                                        while (rs.next()) {
                                    %>
                                    <tr class="row2">
                                        <td><%= rs.getDate("Date")%></td>
                                        <td><%= rs.getInt("old_balance")%></td>
                                        <td><%= rs.getTime("Time")%></td>
                                        <td><%= rs.getInt("id")%></td>
                                        <td><%= rs.getString("OtherAccountName")%></td>
                                        <td><%= rs.getInt("OtherAccountNumber")%></td>
                                        <td><%= rs.getInt("Amount")%></td>
                                        <td><%= rs.getInt("new_balance")%></td>
                                    </tr>
                                    <%
                                        }
                                    } else {%>
                                    <tr class="row2">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>No records found</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <% }
                                        } catch (SQLException e) {

                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                        <div class="titles">New Transaction</div>
                        <div id="transact">
                            <form name="transaction" action="account.jsp" method="POST">
                                <div id="transForm">
                                    <input type="hidden" name="balance" value="<%= bal%>" />
                                    <div class="row">
                                        <div class="parts">
                                            <div class="desc">From account</div>
                                            <div class="row">
                                                <select name="Accs" class="drop">
                                                    <option>Current</option>
                                                    <option>Post Dated</option>
                                                    <option>Trade Dated</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="parts">
                                            <div class="desc">Amount</div>
                                            <input type="text" name="amount" value="" class="txt2"/>
                                        </div>
                                        <div class="parts" style="margin-bottom: 3px">
                                            <div class="desc">Description</div>
                                            <textarea name="desc" rows="1" cols="1" class="txtproj">
                                            </textarea>
                                        </div>
                                        <div style="clear: both"></div>
                                    </div>
                                    <div style="width: 100%; margin-top: 50px;">
                                        <div class="parts">
                                            <div class="desc">Select Beneficiary</div>
                                            <select name="Accs" class="drop">
                                            </select>
                                        </div>
                                        <div class="parts">
                                            <div class="desc">.</div>
                                            <div class="btn">Search Beneficiary</div>
                                        </div>
                                        <div class="parts">
                                            <div class="desc">Beneficiary account Number</div>
                                            <input type="text" name="ben" value="" class="txt2"/>
                                            <input type="submit" value="Transfer" name="btnTrn" class="btn"/>
                                        </div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="../../WEB-INF/static_pages/footer.jsp"></jsp:include>
        </div>
    </body>
</html>
