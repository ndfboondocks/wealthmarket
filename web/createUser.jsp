<%-- 
    Document   : createUser
    Created on : 28-Nov-2014, 13:23:53
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="scripts/Main.css" rel="stylesheet" type="text/css"/>
        <script src="scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui.min.js" type="text/javascript"></script>
        <script src="scripts/mainscript.js" type="text/javascript"></script>
        <title>Basic-Transact</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="WEB-INF/static_pages/head.jsp"></jsp:include>
                <div id="content">
                <div id="profile">Create your User account</div>
                <jsp:include page="WEB-INF/static_pages/createUser.jsp">
                    <jsp:param name="type" value="two" />
                </jsp:include>
                </div>
            <jsp:include page="WEB-INF/static_pages/footer.jsp"></jsp:include>
        </div>
    </body>
</html>
