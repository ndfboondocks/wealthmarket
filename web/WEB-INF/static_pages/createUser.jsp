<%-- 
    Document   : createUser
    Created on : 04-Mar-2015, 17:33:38
    Author     : Stephen
--%>

<%@page import="com.basic.ReadExcel"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String town = "";
    ArrayList<String> states = new ArrayList<String>();
    ArrayList<String> towns = new ArrayList<String>();
    states = ReadExcel.getStates();
    for (int u = 0; u < states.size(); u++) {
        town += "[" + states.get(u) + "]";
        towns = ReadExcel.getTowns(states.get(u));
        for (int j = 0; j < towns.size(); j++) {
            town += "{" + towns.get(j) + "}";
        }
    }
%>
<div id="createForm">
    <div id="usAng">
        <div id="userIcon"></div>
        <div id="terms">
            When you create your account with us. you'd be rich!!!! but well be richer!!!!! hehe...<br /><br /> very nice interface? that's what you get with Stephen! 
        </div>
    </div>
    <div id="formA">
        <form name="create" action="${pageContext.request.contextPath}/CreateUser" method="POST">
            <div class="rows">
                <div class="parts">
                    <div class="desc">First Name</div>
                    <input type="text" name="fnam" value="" class="txt2"/>
                </div>
                <div class="parts">
                    <div class="desc">Last Name</div>
                    <input type="text" name="lnam" value="" class="txt2"/>
                </div>
            </div>
            <div class="rows">
                <div class="parts">
                    <div class="desc">User Name</div>
                    <input type="text" name="unam" value="" class="txt2"/>
                </div>
            </div>
            <div class="rows">
                <div class="parts">
                    <div class="desc">Email</div>
                    <input type="text" name="ema" value="" class="txt2"/>
                </div>
                <div class="parts">
                    <div class="desc">Phone Number</div>
                    <input type="text" name="phone" value="" class="txt2"/>
                </div>
            </div>
            <div class="rows">
                <div class="parts">
                    <div class="desc">Password</div>
                    <input type="password" name="pass" value="" class="txt2"/>
                </div>
                <div class="parts">
                    <div class="desc">Confirm Password</div>
                    <input type="password" name="cpass" value="" class="txt2"/>
                </div>
                <input type="hidden" name="type" value="${param["type"]}" class="txt2"/>
            </div>
            <div class="titles">Address</div>
            <div class="rows">
                <div class="parts" id="address">
                    <div class="desc">State</div>
                    <select name="dd1" class="drop" id="dd1">
                        <%for (int u = 0; u < states.size(); u++) {%><option><%= states.get(u)%></option><%}%>
                    </select>
                </div>
                <div class="parts" id="address">
                    <div class="desc">L.G.A</div>
                    <select id="dd2" name="dd2" class="drop">
                        <option>Please select your state</option>
                    </select>
                </div>
                <div class="parts" id="address">
                    <div class="desc">Town</div>
                    <select id="dd3" name="dd3" class="drop">
                        <option>Please select your l.g.a</option>
                    </select>
                </div>
                <div class="parts" id="address">
                    <div class="desc">Street</div>
                    <select id="dd4" name="dd4" class="drop">
                        <option>Please select your town</option>
                    </select>
                </div>
                <div style="clear: both"></div>
            </div>
            <div style="clear: both"></div>
            <div class="row" style="margin-top: 10px">
                <div class="desc">by clicking "register", you are accepting our terms and conditions</div>
            </div>
            <div class="rows">
                <input type="submit" value="Register" name="btnTrn" class="btn" style="width: 100px"/>
            </div>
        </form>
    </div>
    <div style="clear: both"></div>
</div>
