<%-- 
    Document   : header
    Created on : 09-Dec-2014, 14:28:30
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="header">
    <div>
        <div id="logo">Basic<span class="blue">-transact</span></div>
        <div id="links">
            <a href="../../index.jsp"><div class="link">Logout</div></a>   
            <div class="link">Personal</div> 
            <a href="messages.jsp"><div class="link">Messages</div></a>
            <a href="preferences.jsp"><div class="link">Preferences</div></a>
            <a href="account.jsp"><div class="link">Accounts</div></a>
            <a href="home.jsp"><div class="link">Home</div></a>
        </div>
    </div>
</div>
<div>
    ${param["name"]}
</div>
