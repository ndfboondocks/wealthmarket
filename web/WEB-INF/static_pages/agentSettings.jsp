<%-- 
    Document   : agentSettings
    Created on : 23-Feb-2015, 13:47:07
    Author     : Stephen
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@page import="com.basic.Permissions"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HashMap<Integer, String> perms = new HashMap<Integer, String>();
    String es = ""+session.getAttribute("currentSessionId");
    perms = Permissions.getPermissionGroups();
    Set<Integer> keys = perms.keySet();
    Iterator<Integer> Iterator = keys.iterator();
%>
<div id="content">
    <div id="profile">Settings
        <span class="links">
            <span class="link2" id='adn'>Add Group</span>
            <span class="link2" id="mem">Groups</span>
        </span>
    </div>
    <div id="Allmembers" class="contain">
    </div>
    <div id='adnew' class="contain">
        <%@include file="../jspf/agentGroups.jspf" %>
    </div>
</div>
