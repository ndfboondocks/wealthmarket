<%--
    Document   : messages
    Created on : 30-Dec-2014, 17:14:05
    Author     : Stephen
--%>

<%@page import="com.basic.Login"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<String> body = new ArrayList<String>();
    String id = ""+session.getAttribute("Id");
    Login li = new Login();
    list = li.getMessages(Integer.parseInt(id));
    body = li.getMessageBody();
%>
<div id="content">
    <div id="msgprofile">Messages
        <span class="links">
            <span class="link2" id='po'>Compose</span>
            <span class="link2" id='adn'>Sent Items</span>
            <span class="link2" id="mem">Inbox</span>
        </span>
        <div id="mngItem">
            <div id="itemsLeft">
                <div class="itemsHeader" id="itHead">Inbox <span class="litt">(<%= list.size()%>)</span> <span class="srt">sort ></span><span class="srt">filter ></span></div>
                <div class="itemsHeader" id="tagsHead">Sent Items<span class="srt">sort ></span><span class="srt">filter ></span></div>
                <div id="cnts">
                    <ul>
                        <% int g = list.size();
                    if(g < 1){%>
                        <li class="list0">No messages</li>
                    <%} else{ for(int i = 0; i<list.size(); i++){%>
                        <li class="listu"><div class="msgSub"><%= list.get(i)%></div><div class="msgHide"><%= body.get(i)%></div></li>
                        <% } }%>
                    </ul>
                </div>
            </div>
            <div id="itemsRight">
                <form name="atts" action="index.jsp" method="POST">
                    <div class="itemsHeader">Tools
                        <span class="srt" id="ed">reply ></span>
                        <span class="srt" id="taged">forward ></span>
                        <span class="srt"><input type="submit" value="delete >" name="delBtn" class="srtBtn"/></span>
                    </div>
                    <div id="itemContent">
                        <div id="nee">
                            <input type="text" name="viewTitle" value="Subject" class="tittxt" id="tit" />
                            <textarea name="msgBody" rows="15" cols="1" class="txt" id="tnb" >Message</textarea>
                            <div id="tagDisplay" class="tagtxt">
                                <div id="mis">Sent on:
                                    <div id="listTg">
                                    </div>
                                    <input type="text" name="mi" value="" class="tgtxt" id="mi"/></div>
                            </div>
                            <div class="txt" id="eds">
                                <input type="submit" value="save" name="saveBtn" class="btn"/>
                                <input type="submit" value="cancel" name="cclBtn" class="btn"/>
                            </div>
                            <div class="txt" id="tageds">
                                <input type="submit" value="save tag" name="tagsaveBtn" class="btn"/>
                                <input type="submit" value="cancel" name="tagcclBtn" class="btn" id="can"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>
</div>

