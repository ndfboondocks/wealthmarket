<%-- 
    Document   : staff
    Created on : 28-Dec-2014, 22:28:15
    Author     : Stephen
--%>

<%@page import="com.basic.Login"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> list = new ArrayList<String>();
    ArrayList<String> stafflist = new ArrayList<String>();
    ArrayList<String> email = new ArrayList<String>();
    String as = ""+session.getAttribute("currentSessionUser");
    String id = ""+session.getAttribute("currentSessionId");
    Login li = new Login();
    list = li.getMembers();
    stafflist = li.getStaffList(as);
    email = li.getMembersEmail();
    String mem = "";
    String ema = "";
    for(int o = 0; o<list.size(); o++){
        if(o == list.size() - 1){
            mem += list.get(o);
            ema += email.get(o);
        }
        else{
            mem += list.get(o)+",";
            ema += email.get(o)+",";
        }
    }
%>
<div id="content">
    <div id="profile">Staff
        <span class="links">
            <span class="link2" id='po'>Profile</span>
            <span class="link2">Access</span>
            <span class="link2" id='adn'>Add new</span>
            <span class="link2" id="mem">All Staff</span>
        </span>
    </div>
    <div id="Allmembers" class="contain">
        <div id="search">
            <div class="parts">
                <div class="desc">Staff Name</div>
                <input type="text" name="ben" value="" class="txt2"/>
                <input type="submit" value="Search Staff" name="btnTrn" class="btn"/>
            </div>
            <div style="clear: both"></div>
        </div>
        <div id="all">
            <div class="titles">Staff List</div>
            <div id='list'>
                <ul>
                    <%
                    int g = stafflist.size();
                    if(g < 1){%>
                        <li class="list0">No staff added yet</li>
                    <%}
                    else{
                        for(int i = 0; i<stafflist.size(); i++){%>
                        <li class="list"><%= stafflist.get(i)%></li>
                        <%}
                    }%>
                    
                </ul>
            </div>
                    <input type="hidden" name="members" value="<%= mem%>" id="members"/>
                    <input type="hidden" name="email" value="<%= ema%>" id="email"/>
                    <input type="hidden" name="agent" value="<%= id%>" id="fragent"/>
        </div>
    </div>
        <div id="pro" class="contain">
            <div class="titles">Personal Details</div>
            <div id="pers">
                <div id="pic" class="left"></div>
                <div class="left" id="pros">
                    <div class="row"><span class="init">Name:</span><span class="res" id="nam"></span></div>
                    <div class="row"><span class="init">E-mail Address:</span> <span class="res" id="ema"></span></div>
                    <div class="row"><span class="init">Age:</span><span class="res">24</span></div>
                    <div class="row"><span class="init">Phone Number:</span><span class="res">07084690619</span></div>
                    <div class="row"><span class="init">Home Address:</span><span class="res">Sangotedo, Ajah</span></div>
                </div>
                <div style="clear: both"></div>
            </div>
            <div class="titles">Permissions</div>
            <input type="hidden" name="inam" value="" id="acnam2"/> 
        </div>
    <div id='adnew' class="contain">
        <div style="margin-bottom: 25px">
            <div class="parts">
                <div class="desc">Member Name</div>
                <input type="text" name="ben" value="" class="txt2"/>
            </div>
            <div class="parts">
                <div class="desc">Email address</div>
                <input type="text" name="ben" value="" class="txt2"/>
                <input type="submit" value="Search Member" name="btnTrn" class="btn"/>
            </div>
            <div style="clear: both"></div>
        </div>
        <div>
            <div class="titles">Member List</div>
            <div id='list'>
                <ul>
                    <%
                    for(int i = 0; i<list.size(); i++){%>
                    <li class="list2"><%= list.get(i)%><div class="add"></div></li>
                    <%}
                    %>
                </ul>
            </div>
                    <input type="hidden" name="staff" value="<%= mem%>" id="staff"/>
        </div> 
    </div>
        <div id='adstaff'>
            <div id='staffPan'>
                <%@include file="../jspf/addStaff.jspf"%>
            </div>
        </div>
</div>
