<%-- 
    Document   : adminHeader
    Created on : 11-Dec-2014, 16:24:42
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--This section provides the header for the -->
<div id="header">
    <div>
        <div id="logo">Basic<span class="blue">-transact</span></div>
        <div id="links">
            <a href="${pageContext.request.contextPath}/logoutServlet"><div class="link">Logout</div></a>    
            <div class="link">Messages</div>
            <a href="${pageContext.request.contextPath}/SettingsServlet"><div class="link">Settings</div></a>
            <a href="${pageContext.request.contextPath}/ProjectServlet"><div class="link">Projects</div></a>
            <a href="${pageContext.request.contextPath}/TransactionsServlet"><div class="link">Transactions</div></a>
            <a href="${pageContext.request.contextPath}/AddressServlet"><div class="link">Accounts</div></a>
            <a href="../../members.jsp"><div class="link">Members</div></a>
        </div>
    </div>
</div>
