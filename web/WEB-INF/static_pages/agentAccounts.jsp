<%-- 
    Document   : agentAccounts
    Created on : 27-Feb-2015, 13:52:34
    Author     : Stephen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> list = new ArrayList<String>();
    list = (ArrayList<String>) session.getAttribute("names");
    ArrayList<Integer> permissions = new ArrayList<Integer>();
    permissions = (ArrayList<Integer>) session.getAttribute("permissions");
%>
<div id="content">
    <div id="profile">Account Settings
        <span class="links">
            <span class="link2">Details</span>
           <span class="link2" id="adn">Address</span>
            <span class="link2" id="mem">Agents</span>
        </span>
    </div>
    <div id="Allmembers" class="contain">
        <%@include file="../../WEB-INF/jspf/addAgent.jspf" %>
    </div>
    <div id="adnew">
        <%@include file="../../WEB-INF/jspf/address.jspf" %>
    </div>
</div>

