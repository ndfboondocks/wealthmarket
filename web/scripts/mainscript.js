/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(window).load(function () {
    $('tr:even').css('background-color', '#eee');
    $('#hed').css('background-color', '#3cf');
    $('#pro').hide();
    $("#adnew").hide();
    $("#addtwo").hide();
    $("#edlist").hide();
    $('.subCont').hide();
});
function changeFunc() {
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    alert(selectedValue);
}
$(document).ready(function () {
    $(".list").click(function () {
        $("#edlist").show(500);
        var splitted = $("#members").val().split(",");
        var splitema = $("#email").val().split(",");
        var splitphn = $("#phn").val().split("~");
        var splitadr = $("#adr").val().split("~");
        document.getElementById('nam').innerHTML = $(this).text();
        document.getElementById('name').innerHTML = $(this).text();
        var elem = document.getElementById('acnam2');
        elem.value = $(this).text();
        for (var i = 0; i < splitted.length; i++) {
            if (splitted[i] === $(this).text()) {
                document.getElementById('ema').innerHTML = splitema[i];
                document.getElementById('ph').innerHTML = splitphn[i];
                document.getElementById('ad').innerHTML = splitadr[i];
            }
        }
        var agenti = document.getElementById('agentid2');
        agenti.value = $("#fragent2").val();
    });
    $("#edlist").click(function () {
        $("#po").removeClass("link2");
        $("#po").addClass("link2b");
        $("#adn").removeClass("link2b");
        $("#adn").addClass("link2");
        $("#mem").removeClass("link2b");
        $("#mem").addClass("link2");
        $("#Allmembers").hide(500);
        $("#pro").show(500);
        $("#adnew").hide(500);
    });
    $(".list2").click(function () {
        $("#adstaff").css('visibility', 'visible');
        document.getElementById('nam2').innerHTML = $(this).text();
        var elem = document.getElementById('agnm');
        elem.value = $(this).text();
        var agent = document.getElementById('agentid');
        agent.value = $("#fragent").val();
    });
    $("#adsub").click(function () {
        $("#adstaff").css('visibility', 'visible');
        $("#addtwo").show();
    });
    $('.subHead').click(function () {
        var tes = $('#acnam2').val();
        $(this).parent().children('#acnam').val(tes);
        $('.subCont').hide();
        $(this).parent().children('.subCont').slideToggle();
    });
    $("#addone").click(function () {
        var name = $("#namey").val();
        var rate = $("#rat").val();
        var myTable = document.getElementById("tab");
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var currentCell = currentRow.insertCell(-1);
        currentCell.innerHTML = name;

        currentCell = currentRow.insertCell(-1);
        currentCell.innerHTML = rate;

        currentCell = currentRow.insertCell(-1);
        currentCell.innerHTML = "hello";
        $("#adstaff").css('visibility', 'hidden');
    });
    $("#mem").click(function () {
        $("#mem").removeClass("link2");
        $("#mem").addClass("link2b");
        $("#po").removeClass("link2b");
        $("#po").addClass("link2");
        $("#adn").removeClass("link2b");
        $("#adn").addClass("link2");
        $("#Allmembers").show(500);
        $("#pro").hide(500);
        $("#adnew").hide(500);
    });
    $("#adn").click(function () {
        $("#adn").removeClass("link2");
        $("#adn").addClass("link2b");
        $("#po").removeClass("link2b");
        $("#po").addClass("link2");
        $("#mem").removeClass("link2b");
        $("#mem").addClass("link2");
        $("#adnew").show(500);
        $("#pro").hide(500);
        $("#Allmembers").hide(500);
    });
    $("#startDatePicker").datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: "yy-mm-dd",
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    });
    $("#startDatePicker2").datepicker({
        inline: true,
        showOtherMonths: true,
        dateFormat: "yy-mm-dd",
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    });
    $(".list2").hover(function () {
        $('.add').css({'background-image': 'url("../images/addicon.png")'});
        $(this).children('.add').css({'background-image': 'url("../images/addicon0.png")'});
    });
    $(".list2").mouseleave(function () {
        $('.add').css({'background-image': 'url("../images/addicon.png")'});
    });
    $("#close").click(function () {
        $("#adstaff").css('visibility', 'hidden');
    });
    $("#tab td").click(function (e) {
        if ($(this).find('input').length) {
            return;
        }
        var input = $("<input type='text' size='12' />")
                .val($(this).text());
        $(this).empty().append(input);
        $(this).find('input')
                .focus()
                .blur(function (e) {
                    $(this).parent('td').text(
                            $(this).val()
                            );
                });
    });
    $('#dd1').change(function () {
        fillOptions('dd2', this);
        $('>option', '#dd3').remove();
        $('#dd3').append($('<option/>').text("Please select your l.g.a"));
        $('>option', '#dd4').remove();
        $('#dd4').append($('<option/>').text("Please select your town"));
    });
    $('#dd2').change(function () {
        fillOptions('dd3', this);
        $('>option', '#dd4').remove();
        $('#dd4').append($('<option/>').text("Please select your town"));
    });
    $('#dd3').change(function () {
        fillOptions('dd4', this);
    });
});

function fillOptions(ddId, callingElement) {
    var dd = $('#' + ddId);
    $.getJSON('options?dd=' + ddId + '&val=' + $(callingElement).val(), function (opts) {
        $('>option', dd).remove();
        if (opts) {
            $.each(opts, function (key, value) {
                dd.append($('<option/>').val(key).text(value));
            });
        } else {
            dd.append($('<option/>').text("Please select parent"));
        }
    });
}

